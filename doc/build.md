# Build Azuma from Sources

Tool-chain required:

1. GCC >11
2. Binutils >2.38
3. CMake >3.22.1
4. vcpkg (optional)

Libraries:

1. OpenSSL crypto
2. Boost
3. libconfig
4. fmt
5. catch2 (for unit tests only)
6. cppcodec

Azuma is confirmed to be built successfully in Debian 12 (Bookworm) and
Ubuntu 22.04 LTS. Other Linux distributions (e.g. Fedora) may be supported in the
future. There is no plans to support Windows.

For security reason, the OpenSSL crypto library (libcrypto) will be dynamically
linked. It will be typically provided by the operating system. Other libraries, 
such as Boost, can be obtained by vcpkg.

To build Azuma, run these commands in the project directory:

```bash
cmake -S . -B build \
  -DCMAKE_TOOLCHAIN_FILE="${VCPKG_ROOT}"/scripts/buildsystems/vcpkg.cmake
cmake --build build
```

The `azuma` executable should be generated in the `build/src` directory.

See also:
* [vcpkg configuration file](../vcpkg.json): for version numbers of the dependencies
* [build script](../do_build) used by build pipeline