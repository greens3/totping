//
// Created by nestal on 2/24/24.
//

#include "Azuma.h"
#include "SessionManagement.h"
#include "Version.h"

#include "crypt/IdentityDatabase.h"
#include "crypt/TokenDatabase.h"
#include "crypt/GenericToken.h"
#include "http/Cookie.h"
#include "http/SplitBuffers.h"
#include "util/StringFields.h"
#include "util/Log.h"

#include "Sanali.hh"

#include <iterator>
#include <yaml-cpp/yaml.h>

// For "program_invocation_short_name"
#include <cerrno>

namespace YAML {

template <class Rep, class Period> struct convert<std::chrono::duration<Rep, Period>>
{
	static Node encode(const std::chrono::duration<Rep, Period>& d)
	{
		return Node{d.count()};
	}
	static bool decode(const Node& node, std::chrono::duration<Rep, Period>& d)
	{
		if (node.Type() != NodeType::Scalar)
			return false;

		d = std::chrono::duration<Rep, Period>{node.as<Rep>()};
		return true;
	}
};
}

namespace hrb {
using namespace boost::asio;

namespace {

template <typename T>
T&& lookup_value(const YAML::Node& config, const std::string& field, T&& inout_value)
{
	assert(config.Type() != YAML::NodeType::Null);

	if (auto node = config[field]; node.Type() != YAML::NodeType::Null)
		inout_value = node.as<std::remove_cvref_t<T>>();

	return std::forward<T>(inout_value);
}

template <typename Rep, typename Period>
std::chrono::duration<Rep, Period>&& lookup_value(
	const YAML::Node& config,
	const std::string& field,
	std::chrono::duration<Rep, Period>&& duration
)
{
	auto rep = lookup_value(config, field, duration.count());
	duration = std::chrono::duration<Rep, Period>{rep};
	return std::move(duration);
}
auto parse_address(const YAML::Node& config, const std::string& prefix, std::string addr, unsigned port)
{
	ip::tcp::endpoint result;
	lookup_value(config, prefix + "address", addr);
	result.address(ip::make_address(addr));

	lookup_value(config, prefix + "port", port);
	result.port(static_cast<ip::port_type>(port));

	return result;
}
} // end of local namespace

Configuration::Configuration(const YAML::Node& config) :
	m_listen_addr{parse_address(config, "listen_", "127.0.0.1", 8964)}
{
	lookup_value(config, "redis_host", m_redis_host);
	lookup_value(config, "redis_port", m_redis_port);

	lookup_value(config, "refresh_token_ttl", m_session.refresh_token_ttl);
	lookup_value(config, "access_token_ttl", m_session.access_token_ttl);
	lookup_value(config, "session_id_refresh_before", m_session.session_id_refresh_before);

#ifdef __GLIBC__
	m_db_name.assign(::program_invocation_short_name);
#endif
	lookup_value(config, "realm", m_db_name);
	if (m_db_name.empty())
		throw std::runtime_error("Database name cannot be empty");

	lookup_value(config, "entropy_length", m_session.entropy_length);
	lookup_value(config, "valid_totp_steps", m_totp.valid_totp_steps);

	lookup_value(config, "totp_period", m_totp.period);

	lookup_value(config, "totp_digits", m_totp.digits);

	std::string_view sha1 = ::EVP_MD_get0_name(::EVP_sha1());
	std::string hash{sha1};
	lookup_value(config, "totp_hash", hash);
	m_totp.hash = ::EVP_get_digestbyname(hash.empty() ? sha1.data() : hash.c_str());
	if (!m_totp.hash)
	{
		Log(LOG_WARNING, "Unknown TOTP hash algorithm '{}'. Using default {}.", hash, sha1);
		m_totp.hash = ::EVP_sha1();
	}

	// Read login.html into memory.
	// If the file does not exist, the string will be empty.
	std::string login_filename{"./lib/login.html"};
	lookup_value(config, "login_html", login_filename);
	std::ifstream login_file{login_filename};
	m_login_html.assign(std::istreambuf_iterator<char>{login_file}, {});

	// Construct template with login.html
	StringTemplate tmp{m_login_html};
	tmp.replace("{{****server name****}}");
	tmp.replace(R"(<h4 class="failedp">{{***login-failed***}}</h4>)", "{{***login-failed***}}");
	m_login_srcs.assign(tmp.begin(), tmp.end());
	assert(m_login_srcs.size() <= SplitBuffers<template_count>::value_type::segment_count);
}

void Azuma::login(http::request<http::string_body>&& request, ResponseSender&& send)
{
	if (!request[http::field::content_type].starts_with("application/x-www-form-urlencoded"))
	{
		Log(
			LOG_WARNING,
			"#{}: Unknown content-type '{}'",
			m_request_index, request[http::field::content_type]
		);
		return send(http::response<http::empty_body>{http::status::bad_request, request.version()});
	}

	auto [username, password, totp] = urlform.find(request.body(), "username", "password", "totp");
	Log(LOG_INFO, "#{}: Trying to authenticate user {}", m_request_index, username);

	std::uint32_t totp_num;
	auto [ptr, erc] = std::from_chars(totp.begin(), totp.end(), totp_num);
	if (erc != std::errc{})
	{
		Log(LOG_WARNING, "Invalid TOTP from request '{}'", totp);
		return send(http::response<http::empty_body>{http::status::bad_request, request.version()});
	}

	IdentityDatabase::Credential cred{std::string{username}, Password{url_decode(password)}, totp_num};

	using namespace std::chrono;
	identity_db().authenticate(
		cred, system_clock::now(),
		[
			send=std::move(send), version=request.version(), identity=cred.identity, *this
		](auto ec) mutable
		{
			if (ec)
				return send(http::response<http::empty_body>{status_from_error(ec), version});

			token_db().create_session(
				identity, system_clock::now(),
				[send=std::move(send), *this, version](auto&& session, auto expiry, auto ec) mutable
				{
					http::response<http::empty_body> resp{status_from_error(ec), version};
					if (!ec)
					{
						auto age = duration_cast<seconds>(expiry - system_clock::now());
						resp.set(http::field::set_cookie, set_cookie(session, age).str());
						Log(LOG_INFO, "Session {} created successfully.", session);
					}
					return send(std::move(resp));
				}
			);
		}
	);

}

void Azuma::logout(http::request<http::string_body>&& request, std::string_view redirect, ResponseSender&& send)
{
	auto session = extract_session_id(request);
	if (!session.valid())
	{
		Log(LOG_WARNING, "#{}: Invalid session {} requested for logout", m_request_index, session);
		return send(http::response<http::empty_body>{http::status::bad_request, request.version()});
	}

	token_db().revoke_token(
		session,
		[
			send = std::move(send), version = request.version(), location = std::string{redirect},
			session, req_index = m_request_index
		](auto ec) mutable {
			Log(LOG_INFO, "#{}: Session {} destroyed {} ({})", req_index, session, ec, ec.message());
			return send_logout_response(location, std::move(send), version);
		}
	);
}

void Azuma::logout_all(http::request<http::string_body>&& request, std::string_view redirect, ResponseSender&& send)
{
	auto session = extract_session_id(request);

	token_db().verify_session(
		session, std::chrono::system_clock::now(),
		[
			send = std::move(send), version = request.version(), *this,
			location = std::string{redirect}, username = session.owner()
		](auto&&, auto, auto ec) mutable {
			if (ec == std::errc::permission_denied)
				return send(http::response<http::empty_body>{http::status::unauthorized, version});

			token_db().revoke_all_tokens(
				username, TokenType::session_id,
				[send = std::move(send), version, location](auto) mutable {
					return send_logout_response(location, std::move(send), version);
				}
			);
		}
	);
}

void Azuma::send_logout_response(std::string_view location, ResponseSender&& send, unsigned int version)
{
	http::response<http::empty_body> res{http::status::found, version};
	res.set(http::field::set_cookie, set_cookie(GenericToken{}, {}).str());
	res.set(http::field::location, location.empty() ? "/" : location);
	return send(std::move(res));
}

void Azuma::auth(http::request<http::string_body>&& request, ResponseSender&& send)
{
	auto session = extract_session_id(request);
	token_db().verify_session(
		session, std::chrono::system_clock::now(),
		[send = std::move(send), version = request.version(), session, this](auto&& new_session, auto expiry, auto ec) mutable
		{
			fmt::print("new session type = {}\n", new_session.type());

			if (ec)
				Log(
					LOG_INFO, "#{}: Session {} authentication failure: {}",
					m_request_index, session, ec.message()
				);

			http::response<http::empty_body> resp{status_from_error(ec), version};
			if (!ec && session != new_session && new_session.type() == TokenType::session_id)
			{
				// Token type should not change after refresh.
				assert(new_session.type() == session.type());

				Log(
					LOG_INFO, "#{}: Session {} renewed to {}.",
					m_request_index, session, new_session, ec.message()
				);

				using namespace std::chrono;
				auto age = duration_cast<seconds>(expiry - system_clock::now());
				resp.set(http::field::set_cookie, set_cookie(new_session, age).str());
			}
			return send(std::move(resp));
		}
	);
}

void Azuma::operator()(http::request<http::string_body>&& request, ResponseSender&& real_sender)
{
	m_request_index = m_stat.begin_request();
    Log(LOG_DEBUG,
	    "#{}: {} {} (from {}:{}) (size {} bytes)",
	    m_request_index,
        to_string(request.method()), request.target(),
		request["X-Real-IP"], request["X-Original-URI"], request.body().size()
    );

	// Common header for all responses
	auto send = [&stat=m_stat, sender=std::move(real_sender)](auto&& response) mutable
	{
		stat.end_request();
		response.set(http::field::server, server_header());
		return sender(std::forward<decltype(response)>(response));
	};

	if (m_stat.outstanding() >= m_config.max_concurrent_requests())
	{
		Log(
			LOG_WARNING,
			"Rejecting request #{}: too many outstanding requests {}",
			m_request_index, m_config.max_concurrent_requests()
		);
		return send(http::response<http::empty_body>{http::status::service_unavailable, request.version()});
	}

	if (request.target() == "/login" && request.method() == http::verb::post)
		return login(std::move(request), std::move(send));

	if (request.target() == "/auth" && request.method() == http::verb::get)
		return auth(std::move(request), std::move(send));

	std::string_view query{request.target()};
	auto [path, sep] = split_left(query, "?");
	auto [redirect] = urlform.find(query, "redirect");

	if (path == "/login" && request.method() == http::verb::get)
		return send_login_page({}, std::move(send), request.version());

	if (path == "/logout" && request.method() == http::verb::get)
		return logout(std::move(request), redirect, std::move(send));

	if (path == "/logout_all" && request.method() == http::verb::get)
		return logout_all(std::move(request), redirect, std::move(send));

	return send(http::response<http::empty_body>{http::status::not_found, request.version()});
}

void Azuma::send_login_page(std::string failure, ResponseSender&& send, unsigned version)
{
	http::response<hrb::SplitBuffers<template_count>> result{
		std::piecewise_construct,
		std::make_tuple(m_config.login_srcs().begin(), m_config.login_srcs().end()),
		std::make_tuple(http::status::ok, version)
	};
	result.body().set_extra(0, m_config.database_name());
	result.body().set_extra(1, std::move(failure));

	result.prepare_payload();
	return send(std::move(result));
}

TokenDatabase Azuma::token_db()
{
	return TokenDatabase{m_redis, m_config.database_name(), m_config.session()};
}

IdentityDatabase Azuma::identity_db()
{
	return IdentityDatabase{
		m_redis, m_config.database_name(),
		m_config.session(),
		m_config.totp()
	};
}

http::status Azuma::status_from_error(std::error_code ec, http::status ok)
{
	if (!ec)
		return ok;

	if (ec == std::errc::permission_denied)
		return http::status::unauthorized;

	if (ec == std::errc::invalid_argument)
		return http::status::bad_request;

	if (ec == std::errc::device_or_resource_busy)
		return http::status::service_unavailable;

	return http::status::internal_server_error;
}

} // end of namespace
