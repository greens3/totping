//
// Created by nestal on 2/24/24.
//

#pragma once

#include "crypt/TOTP.h"
#include "crypt/AuthenticationConfig.h"
#include "http/HTTP.h"
#include "Sanali.hh"

#include <boost/asio/ip/basic_endpoint.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <chrono>
#include <cstddef>
#include <string>

namespace YAML {class Node;}

namespace hrb {

class Password;
class IdentityDatabase;
class TokenDatabase;

class Configuration
{
public:
	explicit Configuration(const YAML::Node& config);

	auto& listen_addr() const {return m_listen_addr;}
	auto& redis_host() const {return m_redis_host;}
	auto& redis_port() const {return m_redis_port;}

	auto session_length() const {return m_session_length;}
	auto session_key_length() const {return m_session_key_length;}

	auto max_concurrent_requests() const {return m_max_concurrent_requests;}

	// Used as prefixes for keys in redis database.
	// Also used as issuer for authenticator key URI.
	// See https://github.com/google/google-authenticator/wiki/Key-Uri-Format
	auto database_name() const {return m_db_name;}

	auto& totp() const {return m_totp;}
	auto& session() const {return m_session;}

	auto& login_srcs() const {return m_login_srcs;}

private:
	boost::asio::ip::tcp::endpoint m_listen_addr;
	std::string m_redis_host{"127.0.0.1"};
	std::string m_redis_port{sanali::redis_port};

	std::string m_db_name;
	std::chrono::seconds    m_session_length{std::chrono::days{1}};
	std::uint64_t           m_session_key_length{24};
	std::uint64_t           m_max_concurrent_requests{1024};

	TOTPConfig      m_totp;
	AuthenticationConfig   m_session;

	// For generated the login.html with templates
	std::string m_login_html;
	std::vector<std::string_view> m_login_srcs;
};

class SharedStatistic
{
public:
	SharedStatistic() = default;

	auto begin_request() {++m_outstanding; return m_total_request++;}
	void end_request() {--m_outstanding;}

	auto total_request() const {return m_total_request.load();}
	auto outstanding() const {return m_outstanding.load();}

private:
	std::atomic_size_t  m_total_request{};
	std::atomic_size_t  m_outstanding{};
};

class Azuma
{
public:
	Azuma(sanali::Redis redis, const Configuration& config, SharedStatistic& stat) :
		m_config{config},
		m_redis{std::move(redis)},
		m_stat{stat}
	{
	}

	void operator()(http::request<http::string_body>&& request, ResponseSender&& send);
	TokenDatabase token_db();
	IdentityDatabase identity_db();

private:
	void login(http::request<http::string_body>&& request, ResponseSender&& send);
	void auth(http::request<http::string_body>&& request, ResponseSender&& send);
	void logout(http::request<http::string_body>&& request, std::string_view redirect, ResponseSender&& send);
	void logout_all(http::request<http::string_body>&& request, std::string_view redirect, ResponseSender&& send);

	void send_login_page(std::string failure, ResponseSender&& send, unsigned version);
	static void send_logout_response(std::string_view location, ResponseSender&& send, unsigned version);

	static http::status status_from_error(std::error_code ec, http::status ok = http::status::ok);

private:
	const Configuration&    m_config;
	sanali::Redis           m_redis;
	SharedStatistic&        m_stat;
	std::size_t             m_request_index{};
};

} // end of namespace
