configure_file(Version.cpp.in Version.cpp)

# Share with unit test driver
add_library(azuma_lib
	http/Listener.h
	util/Log.h
	http/Connection.h
	http/HTTP.h
	http/Connection.cpp
	http/Listener.cpp
	crypt/Password.h
	crypt/Password.cpp
	crypt/TokenDatabase.h
	crypt/TokenDatabase.cpp
	crypt/Random.h
	crypt/GenericToken.h
	crypt/GenericToken.cpp
	crypt/qrcodegen.hpp
	crypt/qrcodegen.cpp
	crypt/TOTP.h
	crypt/TOTP.cpp
	crypt/AuthenticationConfig.h
	util/Escape.h
	util/RepeatingTuple.h
	util/Escape.cpp
	util/StringFields.h
	http/Cookie.h
	http/Cookie.cpp
	util/Log.cpp
	http/SplitBuffers.h
	util/StringTemplate.h
	util/StringTemplate.cpp
	util/StringCase.h
	util/RenderQrCode.cpp
	util/RenderQrCode.h
	util/Buffer.h
		SessionManagement.h
		SessionManagement.cpp
	Azuma.cpp
	Azuma.h
	crypt/IdentityDatabase.cpp
	crypt/IdentityDatabase.h
)

target_include_directories(azuma_lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${cppcodec_SOURCE_DIR})
target_link_libraries(azuma_lib PUBLIC
	yaml-cpp
	OpenSSL::Crypto
	Boost::system
	Boost::asio
	fmt::fmt
	sanali
)

# Server executable
add_executable(azuma
	main.cpp
	${CMAKE_CURRENT_BINARY_DIR}/Version.cpp
)
target_link_libraries(azuma PUBLIC azuma_lib Boost::program_options)

add_dependencies(azuma run_unittests)
