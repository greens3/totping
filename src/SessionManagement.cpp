//
// Created by root on 3/27/24.
//

#include "SessionManagement.h"
#include "crypt/GenericToken.h"

#include "http/HTTP.h"
#include "http/Cookie.h"

#include "util/Escape.h"

#include <cppcodec/base64_rfc4648.hpp>

namespace hrb {

using base64 = cppcodec::base64_rfc4648;

constexpr std::string_view cookie_id{"id"};

GenericToken extract_session_id(const http::header<true, boost::beast::http::fields>& request)
{
	if (auto cookie = request[http::field::cookie]; !cookie.empty())
		if (auto result = GenericToken{}.from_string(Cookie{cookie}.field(cookie_id));
			result.type() == TokenType::session_id)
			return result;

	// https://datatracker.ietf.org/doc/html/rfc2617#page-5
	if (auto b_auth = request[http::field::authorization]; !b_auth.empty())
	{
		std::string_view auth{b_auth.data(), b_auth.size()};
		auto [basic, sep] = split_left(auth, " ");
		if (basic == "Basic" && sep == ' ')
		{
			auto b64{base64::decode(auth)};
			if (auto result = GenericToken{}.from_string(ConstBuffer{b64}.string_view());
				result.type() == TokenType::access_token)
				return result;
		}
	}

	return {};
}

Cookie cookie(const GenericToken& session)
{
	Cookie cookie;
	cookie.add(cookie_id, session.valid() ? session.to_string() : "");
	return cookie;
}

Cookie set_cookie(const GenericToken& session, std::chrono::seconds session_length)
{
	auto cookie{hrb::cookie(session)};
	cookie.add("Path", "/");
	cookie.add("SameSite", "Strict");
	if (session.valid())
	{
		cookie.add("Secure");
		cookie.add("HttpOnly");
		cookie.add("Max-Age", std::to_string(session_length.count()));
	}
	else
	{
		cookie.add("Expires", "Thu, Jan 01 1970 00:00:00 UTC");
	}
	return cookie;
}


} // end of namespace
