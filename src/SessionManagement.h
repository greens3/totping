//
// Created by root on 3/27/24.
//

#pragma once

#include <boost/beast/http/message.hpp>
#include <tuple>

namespace hrb {

enum class TokenType;

class GenericToken;
class Cookie;

GenericToken extract_session_id(
	const boost::beast::http::header<true, boost::beast::http::fields>& request
);

Cookie cookie(const GenericToken& session);
Cookie set_cookie(const GenericToken& session, std::chrono::seconds session_length);

} // end of namespace hrb
