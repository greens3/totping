//
// Created by nestal on 05/03/24.
//

#pragma once

#include <string_view>

namespace hrb {

std::string_view version();
std::string_view server_header();

} // end of namespace hrb
