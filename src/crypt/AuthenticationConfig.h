//
// Created by root on 3/26/24.
//

#pragma once

#include <chrono>
#include <cstddef>

namespace hrb {

struct AuthenticationConfig
{
	// OWASP requires 64-bits of entropy:
	// https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html
	std::size_t             entropy_length{16};

	std::size_t             max_concurrent{10};
	std::chrono::seconds    refresh_token_ttl{std::chrono::days{5}};
	std::chrono::seconds    access_token_ttl{std::chrono::days{1}};
	std::chrono::seconds    session_id_refresh_before{access_token_ttl * 3 / 4};

	std::size_t             authentication_rejection_count{5};
	std::chrono::seconds    min_authentication_attempt_interval{1};
	std::chrono::seconds    authentication_three_strike_interval{900};
};

} // end of namespace hrb