/*
	Copyright © 2024 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 2/18/24.
//

#include "GenericToken.h"
#include "Random.h"
#include "util/Escape.h"
#include "util/StringCase.h"

#include <boost/algorithm/hex.hpp>
#include <cppcodec/base64_rfc4648.hpp>

namespace hrb {

std::string_view to_string(TokenType type)
{
	switch (type)
	{
	using enum TokenType;
	case session_id:    return "session_id";
	case access_token:  return "access_token";
	case refresh_token: return "refresh_token";
	default:            return "none";
	}
}

// Construct Token with specified session key.
GenericToken::GenericToken(std::string owner, TokenType type) : m_owner{to_lower(std::move(owner))}
{
	set_type(type);
}

// See RFC 7617 for the format.
// https://datatracker.ietf.org/doc/html/rfc7617#page-3
GenericToken& GenericToken::from_string(std::string_view val) &
{
	// Use split_right() because we should not assume owner has no colon ":".
	// We can assume the session key has no colon, because it's base64 encoded.
	auto [data64, sep] = split_right(val, ":");
	if (sep == ':')
	{
		m_owner = val;
		set_data(cppcodec::base64_rfc4648::decode(data64));
	}
	return *this;
}

std::string GenericToken::to_string() const
{
	return fmt::format("{}:{}", m_owner, cppcodec::base64_rfc4648::encode(m_data));
}

ConstBuffer GenericToken::entropy() const
{
	assert(m_data.size() >= sizeof(Fields));
	auto size = m_data.size() - sizeof(Fields);
	return {size > 0 ? &m_data[sizeof(Fields)] : nullptr, size};
}

std::string GenericToken::entropy_short(std::size_t length) const
{
	std::string result;
	auto key = entropy().span().first(std::min(length, entropy().size()));
	boost::algorithm::hex(key.begin(), key.end(), std::back_inserter(result));
	return result;
}

GenericToken& GenericToken::set_entropy(ConstBuffer entropy) &
{
	m_data.resize(sizeof(Fields) + entropy.size());
	if (entropy.size() > 0)
		std::memcpy(&m_data.at(sizeof(Fields)), entropy.data(), entropy.size());
	return *this;
}

GenericToken& GenericToken::randomize_entropy(std::size_t entropy_size) &
{
	m_data.resize(sizeof(Fields) + entropy_size);
	if (entropy_size > 0)
		secure_random(&m_data.at(sizeof(Fields)), entropy_size);
	return *this;
}

std::size_t GenericToken::sequence_number() const
{
	std::size_t result{fields().index_msb};
	result <<= 16;
	return result | fields().index.value();
}

GenericToken& GenericToken::set_sequence_number(std::size_t index) &
{
	fields().index     = static_cast<std::uint16_t>(index & 0xffff);
	fields().index_msb = static_cast<std::uint8_t>((index >> 16) & 0xffff);
	return *this;
}

std::ostream& operator<<(std::ostream& os, const GenericToken& tok)
{
	return os << fmt::format("{}", tok);
}

} // end of namespace
