/*
	Copyright © 2024 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 2/18/24.
//

#pragma once

#include "util/Buffer.h"
#include <boost/endian/buffers.hpp>

#include <fmt/format.h>

#include <span>
#include <string>
#include <vector>

namespace hrb {

enum class TokenType {
	none,
	session_id,
	access_token,
	refresh_token
};
std::string_view to_string(TokenType type);
constexpr bool has_parent(TokenType type)
{
	return type == TokenType::session_id || type == TokenType::access_token;
}

/// A token represents an identity in the whole authentication/authorization flows. Different types
/// of tokens are used to present the same identity in different situations.
///
/// It can represent:
/// 	1.	The session ID in "id=" cookie for web sessions
///		2.	The password in RFC7617 HTTP basic authentication, either used by
///			end-users or OAuth clients (note that OAuth clients are typically web servers).
///     3.  OAuth authorization code
///		4.	OAuth access token
///     5.  OAuth refresh token
///
/// It is the primary key to look up the above entities in the database.
///
/// It is consist of the following components:
///
///		1.	Owner:  the name of the identity who create this token. By design, this identity
///         is also the one who uses this token. An _identity_ is either an end-user or an OAuth
///         client. For end-user, identity name is the username. For OAuth client, the identity
///         name is the OAuth client ID.
///		2.	Token index: the (zero-base) nth token created for this identity. For example, if this token is
///         the first token created by an end-user, its index is 0. The existence of this field
///         ensure uniqueness of tokens. It also makes the logs easier to read.
///		3.	Refresh count: the number of times this token has been refreshed. Session IDs has
/// 		grace period before it expires. Within the grace period, if a session ID is used, it
/// 		will be refreshed. A new session ID will be generated and will replace the old one.
///			A session can only be refreshed a limited number of times.
///		4.	Entropy: at least 16 bytes of randomly-generated data. Its purpose is to make tokens
///         unpredictable.
///
/// It is used in the following workflows:
///
///		1.	Authenticate users when they logs in with credentials (identity + password +TOTP token)
///			This class represents the session ID returned by Azuma.
///         "Owner" field in this class will be the end-user.
/// 		end-user -------------(credentials)--------------> Azuma (as authorization server)
///			         <------(session ID or auth code)--------- (authentication endpoint)
///
///		2.	OAuth client register themselves to Azuma, which generates a secret for them.
///			This class represents the generated secret. "Owner" field in this class will
/// 		be the OAuth client. Note that this is not (yet) performed by HTTP requests.
///			This is likely to be done in command line where the password will be printed in the
///			terminal as an QR code.
/// 		OAuth client -------client ID/redirect URI-------> Azuma (as authorization server)
///			             <----------(client secret)-----------
///
///		3.	OAuth client requests for an access token. This class presents
///			- OAuth client secret: sent by OAuth client, via RFC7617 basic authentication as password
///                                (owner is OAuth client)
///			- Authorization code: sent by OAuth client (obtained in 1) (owner is end-user)
///			- Access token:       sent by Azuma (owner is OAuth client)
/// 		OAuth client --------auth code/redirect URI------> Azuma (as authorization server)
///			             <-----------(Access Token)----------- (token endpoint)
///
///		4.	OAuth client request for a resource with an access token.
///			Azuma plays the role of the resources server in this case.
///			This class represents the access token. "Owner" field is OAuth client.
/// 		OAuth client ------------(Access Token)----------> Azuma (as resources server)
///			             <-------------(Resource)------------- (introspect endpoint)
///
///		5.	Resources servers introspects the access token if it is valid.
///			Azuma plays the role of the authorization server in this case.
///			See RFC 7662. This class represents the access token.  "Owner" field
/// 		is OAuth client.
///			Resource server ---------(Access Token)----------> Azuma (as authorization server)
///			                <-----(active=true/false)--------- (introspect endpoint)
///
class GenericToken
{
public:
	using Data = std::vector<std::uint8_t>;

public:
	GenericToken() = default;
	explicit GenericToken(std::string owner, TokenType type = TokenType::none);

	GenericToken& from_string(std::string_view val) &;
	auto&& from_string(std::string_view val) && {return std::move(from_string(val));}
	std::string to_string() const;

	std::size_t sequence_number() const;
	GenericToken&  set_sequence_number(std::size_t index) &;
	auto&&         set_sequence_number(std::size_t index) && {return std::move(set_sequence_number(index));}

	GenericToken& set_entropy(ConstBuffer entropy) &;
	auto&& set_entropy(ConstBuffer entropy) && {return std::move(set_entropy(entropy));}
	GenericToken& randomize_entropy(std::size_t entropy_size) &;
	auto&& randomize_entropy(std::size_t entropy_size) && {return std::move(randomize_entropy(entropy_size));}

	auto type() const {return static_cast<TokenType>(fields().type);}
	auto&  set_type(TokenType t) &  {fields().type = static_cast<std::uint8_t>(t); return *this;}
	auto&& set_type(TokenType t) && {return std::move(set_type(t));}
	auto owner() const {return m_owner;}
	auto& data() const {return m_data;}
	auto& set_data(Data&& data)
	{
		m_data = std::move(data);
		if (m_data.size() < sizeof(Fields))
			m_data.resize(sizeof(Fields));
		return *this;
	}
	auto& set_data(const Data& data) {return set_data(Data(data));}
	auto& set_data(std::string_view data) {return set_data(ConstBuffer{data}.to_vec());}
	auto& set_data() {return set_data(Data{});}

	ConstBuffer entropy() const;
	std::string entropy_short(std::size_t length= 2) const;

	bool valid() const {return type() != TokenType::none && !m_owner.empty() && m_data.size() > sizeof(Fields);}

	auto operator<=>(const GenericToken&) const = default;

private:
	struct Fields
	{
		std::uint8_t    type;
		std::uint8_t    index_msb;
		boost::endian::little_uint16_buf_t  index;
	};
	static_assert(sizeof(Fields) == 4);
	const Fields& fields() const
	{
		assert(m_data.size() >= sizeof(Fields));
		return *reinterpret_cast<const Fields*>(m_data.data());
	}
	Fields& fields()
	{
		assert(m_data.size() >= sizeof(Fields));
		return *reinterpret_cast<Fields*>(m_data.data());
	}

private:
	std::string m_owner;
	Data        m_data{Data(sizeof(Fields), {})};   // Ensure large enough for storing Fields
};

std::ostream& operator<<(std::ostream& os, const GenericToken& tok);

} // end of namespace hrb

namespace fmt {

template <>
struct formatter<hrb::TokenType>
{
	static constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

	template <typename FormatContext>
	static auto format(hrb::TokenType t, FormatContext& ctx)
	{
		return fmt::format_to(ctx.out(), "{}", to_string(t));
	}
};

template <>
struct formatter<hrb::GenericToken>
{
	static constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

	template <typename FormatContext>
	static auto format(const hrb::GenericToken& s, FormatContext& ctx)
	{
		return s == hrb::GenericToken{} ?
			fmt::format_to(ctx.out(), "<none>") :
			fmt::format_to(
				ctx.out(), "{}:{}:{}{}",
				s.owner(), s.sequence_number(), to_string(s.type()).substr(0, 1),
				s.entropy_short()
			);
	}
};

} // end of namespace fmt