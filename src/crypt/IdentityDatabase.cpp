//
// Created by nestal on 3/30/24.
//

#include "IdentityDatabase.h"

#include "AuthenticationConfig.h"
#include "GenericToken.h"
#include "Random.h"

#include "util/Buffer.h"
#include "util/Log.h"
#include "util/StringCase.h"

#include <cassert>

namespace hrb {

IdentityEntry::IdentityEntry(const Password& password, std::size_t iteration, std::string hash_algo) :
	m_salt{insecure_random<decltype(m_salt)>()},
	m_iteration{iteration == 0 ? min_iteration : iteration},
	m_hash_algorithm{std::move(hash_algo)},
	m_password_hash{password.derive_key(buffer(m_salt), m_iteration, m_hash_algorithm)}
{
}

bool IdentityEntry::verify(const Password& password)
{
	auto pkey = password.derive_key(buffer(m_salt), m_iteration, m_hash_algorithm);
	return pkey.size() == m_password_hash.size() &&
		::CRYPTO_memcmp(pkey.data(), m_password_hash.data(), pkey.size()) == 0;
}

IdentityEntry::IdentityEntry(const std::string& salt, std::size_t iter, std::string algo, const std::string& hash) :
	m_iteration{iter}, m_hash_algorithm{std::move(algo)}
{
	assert(salt.size() == m_salt.size());
	assert(hash.size() == hash.size());
	std::copy(salt.begin(), salt.end(), m_salt.begin());
	std::copy(hash.begin(), hash.end(), m_password_hash.begin());
}

void IdentityDatabase::add_identity(std::string identity, const Password& password, const TOTP& totp, Callback&& comp)
{
	to_lower(identity);

	// Check if identity contain invalid characters
	if (std::find_if(identity.begin(), identity.end(), [](char c)
	{
		return !(std::isalnum(c) || c == '_');
	}) != identity.end())
	{
		Log(LOG_ERR, "Cannot create identity '{}' with invalid characters", identity);
		return comp(make_error_code(std::errc::invalid_argument));
	}

	IdentityEntry user{password};
	m_redis->command(
		[comp=std::move(comp), totp, identity](auto&&, auto ec) mutable
		{
			Log(LOG_INFO, "Created identity '{}' ({})", identity, ec.message());
			comp(ec);
		},
		"HSET {}:identity:{} salt {} iteration {} algorithm {} password {} totp_secret {} totp_counter 0",
		m_prefix, identity,
		buffer(user.salt()),
		user.iteration(), user.hash_algorithm(),
		buffer(user.password_hash()),
		buffer(totp.secret())
	);
}

void IdentityDatabase::remove_identity(const std::string& identity, Callback&& comp)
{
	m_redis->command(
		[comp=std::move(comp)](auto&&, auto ec) mutable
		{
			comp(ec);
		},
		"DEL {}:identity:{}", m_prefix, to_lower_copy(identity)
	);
}

void IdentityDatabase::get_totp(std::string identity, TOTPCallback&& comp)
{
	to_lower(identity);
	auto callback = [comp=std::move(comp), identity](auto&& resp, auto ec) mutable
	{
		if (ec)
			return comp(TOTP{}, ec);

		if (resp.is_string())
			return comp(TOTP{buffer(resp.as_string())}, ec);

		if (resp.is_nil())
			Log(LOG_INFO, "Identity {} not found.", identity);
		else
			Log(LOG_WARNING, "Invalid type of totp_secret field in identity '{}' entry: {}", identity, resp);

		return comp(TOTP{}, make_error_code(std::errc::invalid_argument));
	};
	m_redis->command(std::move(callback), "HGET {}:identity:{} totp_secret", m_prefix, identity);
}

void IdentityDatabase::authenticate(
	Credential cred,
	TimePoint now,
	Callback&& comp
)
{
	to_lower(cred.identity);
	auto callback = [cred, comp=std::move(comp), now, *this](auto&& resp, auto ec) mutable
	{
		if (ec || resp.is_error())
		{
			// If should_authenticate() determines that we can't proceed, it will pass EBUSY.
			// This is not a database error, so we don't need to log.
			if (ec != std::errc::device_or_resource_busy)
				Log(LOG_WARNING, "Error getting identity entry from redis: {} {}", resp, ec);
			// This should not happen: sanali should set "ec" if it returns a Resp with is_error()==true.
			if (!ec)
				ec = make_error_code(std::errc::io_error);
			return comp(ec);
		}
		if (!ec && resp.is_array() && resp.array_size() == 5)
		{
			const auto& [salt, iter, algo, hash_db, totp_db] = resp.template as_tuple<5>(ec);
			if (ec)
			{
				// This should not happen because resp is an 5-item array.
				Log(LOG_WARNING, "Invalid identity entry from redis: {} {}", resp, ec);
				return comp(ec);
			}

			// HMGET will return a list of nil values if the key does not exist.
			if (salt.is_nil() || iter.is_nil() || algo.is_nil() || hash_db.is_nil())
			{
				Log(LOG_INFO, "Authentication failed: identity '{}' does not exist.", cred.identity);
				return comp(make_error_code(std::errc::permission_denied));
			}
			if (!salt.is_string() || salt.as_string().size() != IdentityEntry::Salt{}.size())
			{
				Log(LOG_WARNING, "Invalid salt for identity {}: {}", cred.identity, salt.to_string());
				return comp(std::make_error_code(std::errc::invalid_argument));
			}
			if (!hash_db.is_string() || hash_db.as_string().size() != IdentityEntry::Hash{}.size())
			{
				Log(LOG_WARNING, "Invalid password hash for identity {}: {}", cred.identity, hash_db.to_string());
				return comp(std::make_error_code(std::errc::invalid_argument));
			}

			std::optional<std::int64_t> totp_counter{};
			if (totp_db.is_string())
			{
				using namespace std::chrono;
				TOTP totp_validator{buffer(totp_db.as_string())};
				if (totp_validator.valid())
					totp_counter = totp_validator.validate(cred.totp_token, time_point_cast<seconds>(now), m_totp_config);
			}
			if (!totp_counter)
			{
				Log(
					LOG_INFO, "Authentication failed: TOTP code for identity {} does not match: {}",
					cred.identity, cred.totp_token
				);
				return reject_authentication(
					cred.identity,
					now,
					make_error_code(std::errc::permission_denied),
					std::move(comp));
			}

			try
			{
				IdentityEntry user{salt.as_string(), static_cast<std::size_t>(iter.to_int()), algo.as_string(), hash_db.as_string()};
				if (user.verify(cred.password))
					return ensure_totp_unique(
						*totp_counter, cred.identity,
						[comp=std::move(comp), now, id=cred.identity, *this](auto ec) mutable
						{
							if (ec)
								reject_authentication(id, now, ec, std::move(comp));

							else
								comp(ec);
						}
					);
				else
				{
					Log(LOG_INFO, "Authentication failure: password for identity {} is incorrect", cred.identity);
					return reject_authentication(
						cred.identity, now,
						make_error_code(std::errc::permission_denied),
						std::move(comp)
					);
				}
			}
			catch (std::exception& e)
			{
				Log(LOG_WARNING, "Exception: {}", e.what());
				return comp(make_error_code(std::errc::io_error));
			}
		}

		return comp(make_error_code(std::errc::io_error));
	};

	// First we should check if the user can try login, before really start the login process.
	should_authenticate(
		cred.identity, now, [id = cred.identity, *this, callback = std::move(callback)](auto ec) mutable {
			if (!ec)
				m_redis->command(
					std::move(callback),
					"HMGET {}:identity:{} salt iteration algorithm password totp_secret",
					m_prefix, id
				);

			else
				callback(sanali::Resp{}, make_error_code(std::errc::device_or_resource_busy));
		}
	);
}

/// the "totp_counter" field in the :identity: table stores the number of periods (steps) from the unix epoch
/// of the last used TOTP token. Any TOTP token with counter values less than or equal to the current one
/// will be rejected. The purpose of such logic is to ensure each TOTP token can only be used once.
/// After a successful authentication, the TOTP counter will be stored to the :identity: table to ensure the
/// next authentication attempt will have to use another TOTP token.
void IdentityDatabase::ensure_totp_unique(std::int64_t totp_counter, std::string identity, Callback&& comp)
{
	auto next = [comp=std::move(comp), identity, totp_counter, *this](sanali::Resp&& resp, auto ec) mutable
	{
		if (ec)
		{
			Log(LOG_WARNING, "Error updating TOTP counter for identity {} in redis: {} {}", identity, resp, ec);
			return comp(ec);
		}

		// LUA return false. The TOTP token have been used. Reject the login.
		if (resp.is_nil())
		{
			Log(
				LOG_WARNING, "Authentication failed: TOTP counter {} already used for identity {}.",
				totp_counter, identity
			);
			return comp(make_error_code(std::errc::permission_denied));
		}

		return comp(ec);
	};

	// Note that the `v < ARGV[1]` line in LUA script is very important.
	// It must not be `<=` otherwise the same token will be accepted more than once.
	const char lua[] = R"(
		local v = redis.call('HGET', KEYS[1], 'totp_counter')
		if v < ARGV[1] then
			redis.call('HSET', KEYS[1], 'totp_counter', ARGV[1])
			return true
		else
			return false
		end
	)";
	m_redis->command(std::move(next), "EVAL {} 1 {}:identity:{} {}", lua, m_prefix, identity, totp_counter);
}

void IdentityDatabase::reject_authentication(
	std::string_view username, TimePoint now,
	std::error_code ec, Callback&& comp
)
{
	auto milli_now = std::chrono::time_point_cast<std::chrono::milliseconds>(now);

	const char lua[] = R"(
		local added = redis.call('LPUSH', KEYS[1], ARGV[1])
		local excess = redis.call('LLEN', KEYS[1]) - ARGV[2]
		if excess > 0 then
			redis.call('RPOP', KEYS[1], excess)
		end
		redis.call('EXPIRE', KEYS[1], ARGV[3])
		return excess
	)";

	m_redis->command(
		[comp=std::move(comp), reject_error=ec](auto&& resp, auto ec) mutable
		{
			if (ec)
				Log(LOG_WARNING, "Redis error when rejecting login: {} {} ({})", resp, ec, ec.message());

			return comp(reject_error);
		},
		"EVAL {} 1 {}:rejected:{} {} {} {}",
		lua,                                    // the script
		m_prefix, username,                     // KEYS[1]
		milli_now.time_since_epoch().count(),   // ARGV[1]
		m_session_config.authentication_rejection_count*2,              // ARGV[2]
		m_session_config.authentication_three_strike_interval.count()   // ARGV[3]
	);
}

/// Check if we should try to authenticate an identity by verifying her credentials.
/// If the user has failed to authenticate very recently, we should not even try to authenticate now.
/// This is to prevent an attack who tries to brute-force the identity's credentials.
/// Note that the user may not be even a valid user.
void IdentityDatabase::should_authenticate(std::string_view username, TimePoint now, Callback&& comp)
{
	auto next = [comp=std::move(comp), now, *this, username=std::string(username)](auto&& resp, auto ec) mutable
	{
		if (ec || !resp.is_array())
			Log(LOG_WARNING,
				"Redis error when checking user {} login failure attempts: {} {} ({})",
				username, resp, ec, ec.message()
			);

		assert(resp.array_size() <= m_session_config.authentication_rejection_count);

		std::size_t number_of_strikes{};

		// Do not assume the time stamps in the array is sorted. Always scan the whole array.
		// Even if the time stamps are pushed to the redis list chronologically, it is possible
		// that the system_clock is changed by the system administrator. In such case,
		// system_clock::now() will return a value that is smaller than before, and thus the
		// time stamps in the redis list may not be sorted.
		for (auto&& var : resp)
		{
			// It's a database error if the value stored in redis can't be converted to a number.
			TimePoint attempt{std::chrono::milliseconds{var.to_int(ec)}};
			if (ec)
			{
				Log(LOG_WARNING,
					"Database error: cannot convert last login failure timestamp from '{}' to integers: {} ({})",
					var, ec, ec.message()
				);
				return comp(ec);
			}

			if (auto ago = now - attempt; ago < m_session_config.min_authentication_attempt_interval)
				return comp(make_error_code(std::errc::device_or_resource_busy));

			else if (ago < m_session_config.authentication_three_strike_interval)
			{
				// Three strikes! Too many (authentication_rejection_count) login failures in a
				// period (authentication_three_strike_interval).
				if (++number_of_strikes >= m_session_config.authentication_rejection_count)
				{
					Log(
						LOG_DEBUG, "Login attempt #{} denied for user {}. Max limit reached",
						number_of_strikes, username
					);
					return comp(make_error_code(std::errc::device_or_resource_busy));
				}
			}
		}

		return comp(ec);
	};

	// First get a list of the user's login failure in the past.
	// Note that index in LRANGE is zero-base, but the end-index is inclusive.
	// Therefore, to get 5 entries, the command should be LRANGE 0 4, and hence
	// need to -1 from authentication_rejection_count.
	m_redis->command(
		std::move(next), "LRANGE {}:rejected:{} 0 {}",
		m_prefix, username, m_session_config.authentication_rejection_count-1
	);
}

} // hrb
