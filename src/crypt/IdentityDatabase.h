//
// Created by nestal on 3/30/24.
//

#pragma once

#include "Password.h"
#include "TOTP.h"

#include <fmt/format.h>

#include <boost/asio/any_completion_handler.hpp>

#include "Sanali.hh"

#include <chrono>
#include <cstdint>
#include <filesystem>
#include <string_view>


namespace hrb {

constexpr std::size_t min_iteration = 5000;
constexpr std::string_view default_hash_algorithm{"sha512"};

struct AuthenticationConfig;
struct TOTPConfig;

class IdentityEntry
{
public:
	using Hash = Password::Key;
	using Salt = std::array<std::uint8_t, 32>;

public:
	IdentityEntry() = default;
	explicit IdentityEntry(const std::string& salt, std::size_t iter, std::string algo, const std::string& hash);
	explicit IdentityEntry(
		const Password& password, std::size_t iteration=0,
		std::string hash_algo = std::string{default_hash_algorithm}
	);
	bool verify(const Password& password);

	auto& salt() const {return m_salt;}
	auto& iteration() const {return m_iteration;}
	auto& hash_algorithm() const {return m_hash_algorithm;}
	auto& password_hash() const {return m_password_hash;}

private:
	Salt        m_salt{};
	std::size_t m_iteration{min_iteration};
	std::string m_hash_algorithm{default_hash_algorithm};
	Hash        m_password_hash{};
};

class IdentityDatabase
{
public:
	using TimePoint = std::chrono::system_clock::time_point;
	using TOTPCallback = boost::asio::any_completion_handler<void(TOTP&&, std::error_code)>;
	using Callback = boost::asio::any_completion_handler<void(std::error_code)>;

public:
	IdentityDatabase(
		sanali::Redis redis, std::string prefix,
		const AuthenticationConfig& session_config,
		const TOTPConfig& totp_config
	) :
		m_prefix{std::move(prefix)}, m_session_config{session_config},
		m_totp_config{totp_config}, m_redis{std::move(redis)}
	{
	}

	void add_identity(std::string identity, const Password& password, const TOTP& totp, Callback&& comp);
	void remove_identity(const std::string& identity, Callback&& comp);

	// TODO: seperate authentication and create token
	struct Credential
	{
		std::string     identity;
		Password        password;
		std::uint32_t   totp_token;
	};
	void authenticate(Credential cred, TimePoint now, Callback && comp);
	void get_totp(std::string identity, TOTPCallback&& comp);

private:
	void reject_authentication(std::string_view username, TimePoint now, std::error_code ec, Callback&& comp);
	void ensure_totp_unique(std::int64_t totp_counter, std::string identity, Callback&& comp);
	void should_authenticate(std::string_view username, TimePoint now, Callback&& comp);

private:
	std::string             m_prefix;
	const AuthenticationConfig&    m_session_config;
	const TOTPConfig&       m_totp_config;
	sanali::Redis           m_redis;
};

} // hrb
