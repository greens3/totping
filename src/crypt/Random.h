//
// Created by nestal on 2/18/24.
//

#include <boost/asio/buffer.hpp>
#include <fmt/format.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#include <type_traits>
#include <stdexcept>

namespace hrb {

template <bool secure>
void generic_random(void* data, std::size_t size)
{
	int result{};
	if constexpr (secure)
		result = ::RAND_priv_bytes(static_cast<std::uint8_t*>(data), static_cast<int>(size));
	else
		result = ::RAND_bytes(static_cast<std::uint8_t*>(data), static_cast<int>(size));

	if (result != 1)
		throw std::runtime_error{fmt::format(
			"OpenSSL {} error: {}",
			(secure ? "RAND_priv_bytes()" : "RAND_bytes()"),
			ERR_get_error()
		)};
}

inline void secure_random(void* data, std::size_t size)   {return generic_random<true>(data, size);}
inline void insecure_random(void* data, std::size_t size) {return generic_random<false>(data, size);}

// Simple wrapper for selecting secure and insecure version of random generation.
template <typename T, bool secure>
T&& generic_random(T&& t = T{})
{
	// Build-in types, arrays and std::array<> etc
	if constexpr (std::is_trivially_copyable_v<std::remove_reference_t<T>>)
		generic_random<secure>(&t, sizeof(t));

	// std::vector and std::string
	else if constexpr (requires{{boost::asio::buffer(t)} -> std::convertible_to<boost::asio::mutable_buffer>;})
	{
		auto mut_buf = boost::asio::buffer(t);
		generic_random<secure>(mut_buf.data(), mut_buf.size());
	}
	else
		static_assert(!std::is_same_v<T,T>, "Does not know how to randomize type");
	return std::forward<T>(t);
}

template <typename T>
T&& secure_random(T&& t = T{}){return generic_random<T, true>(std::forward<T>(t));}

template <typename T>
T&& insecure_random(T&& t = T{}){return generic_random<T, false>(std::forward<T>(t));}

} // end of namespace
