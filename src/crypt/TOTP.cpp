//
// Created by nestal on 3/3/24.
//
#include "TOTP.h"
#include "Random.h"

#include <boost/endian.hpp>

#include <cppcodec/base32_rfc4648.hpp>
#include <fmt/format.h>
#include <openssl/hmac.h>

namespace hrb {

using base32 = cppcodec::base32_rfc4648;

TOTP::Secret TOTP::randomize_secret(std::size_t length)
{
	return secure_random(Secret(length));
}

std::string TOTP::generate_uri(std::string_view username, std::string_view issuer) const
{
	assert(valid());
	return fmt::format("otpauth://totp/{}:%20{}?secret={}", issuer, username, base32::encode(m_secret));
}

TOTP::Token TOTP::hmac_token(std::int64_t counter, const TOTPConfig& config) const
{
	assert(config.hash == EVP_sha1() || config.hash == EVP_sha256() || config.hash == EVP_sha512());
	assert(config.digits >= 6);    // RFC4426 said so

	std::uint8_t hmac_result[EVP_MAX_MD_SIZE];
	unsigned hmac_size = sizeof(hmac_result);

	// RFC 4226 specifies big-endian
	boost::endian::big_int64_buf_t msg{counter};

	::HMAC(config.hash, m_secret.data(), static_cast<int>(m_secret.size()), msg.data(), sizeof(msg), hmac_result, &hmac_size);

	// Dynamic truncation of HMAC into 32-bit integer is defined in RFC4226 page6:
	// https://datatracker.ietf.org/doc/html/rfc4226#page-6
	std::size_t   offset   =  hmac_result[hmac_size-1] & 0xf ;
	std::uint32_t bin_code = (hmac_result[offset]  & 0x7f) << 24
		| (hmac_result[offset+1] & 0xff) << 16
		| (hmac_result[offset+2] & 0xff) <<  8
		| (hmac_result[offset+3] & 0xff) ;

	constexpr std::uint32_t pow10[] = {
		1, 10, 100, 1'000, 10'000, 100'000, 1'000'000, 10'000'000, 100'000'000, 1'000'000'000
	};
	assert(config.digits < std::size(pow10));
	return bin_code % pow10[config.digits];
}

} // end of namespace hrb
