//
// Created by nestal on 3/3/24.
//

#pragma once

#include <openssl/evp.h>

#include <cstdint>
#include <chrono>
#include <optional>
#include <vector>

namespace hrb {

struct TOTPConfig
{
	/// Number of time steps backward and forward for a TOTP token to be considered valid.
	/// RFC 6238 recommends two steps backward and forward, such that with 30 seconds of
	/// time window, maximum time drift between client and server is 89 seconds.
	std::int64_t valid_totp_steps{2};

	/// Time duration between TOTP changes. Default value is 30 seconds, meaning that there will be a
	/// new TOTP code every 30 seconds.
	std::chrono::seconds period{30};

	/// Number of digits in the TOTP code. RFC4426 mandates at least 6 digits.
	std::size_t digits{6};

	/// Hash algorithm to be used to generate TOTP code. It can be SHA1, SHA256 or SHA512.
	/// Default is SHA1 (specified by RFC6238)
	const EVP_MD* hash{EVP_sha1()};
};

class TOTP
{
public:
	using Secret = std::vector<std::uint8_t>;
	using Clock = std::chrono::system_clock;
	using Token = std::uint32_t;

	/// Minimum length of shared secret. According to RFC 4226 section 4, requirement
	/// R6, the secret must be at least 128-bits long.
	static constexpr std::size_t min_secret_length = 16;

public:
	TOTP() = default;
	explicit TOTP(Secret secret) : m_secret{std::move(secret)}
	{
	}
	explicit TOTP(boost::asio::const_buffer buffer) : m_secret(buffer.size())
	{
		std::memcpy(m_secret.data(), buffer.data(), buffer.size());
	}

	// https://github.com/google/google-authenticator/wiki/Key-Uri-Format
	std::string generate_uri(std::string_view username, std::string_view issuer) const;

	static Secret randomize_secret(std::size_t length = min_secret_length);

	auto& secret() const {return m_secret;}

	bool valid() const {return m_secret.size() >= min_secret_length;}

	/// Generate TOTP authentication code.
	/// \param  now     Number of seconds since the UNIX epoch.
	template <class Duration=std::chrono::system_clock::time_point::duration>
	auto auth_token(
		std::chrono::time_point<Clock, Duration> now = Clock::now(),
		const TOTPConfig& config = TOTPConfig{}
	) const
	{
		return hmac_token(now.time_since_epoch()/config.period, config);
	}

	template <class Duration=std::chrono::system_clock::time_point::duration>
	std::optional<std::int64_t> validate(
		Token token,
		std::chrono::time_point<std::chrono::system_clock, Duration> now,
		const TOTPConfig& config = TOTPConfig{}
	)
	{
		auto mid = now.time_since_epoch()/config.period;
		for (auto step = mid-config.valid_totp_steps; step <= mid+config.valid_totp_steps; ++step)
			if (token == hmac_token(step, config))
				return step;

		return std::nullopt;
	}

	auto operator<=>(const TOTP& rhs) const = default;

private:
	Token hmac_token(std::int64_t counter, const TOTPConfig& config = TOTPConfig{}) const;

private:
	Secret      m_secret;
};

} // end of namespace
