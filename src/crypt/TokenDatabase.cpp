/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 9/1/23.
//

#include "TokenDatabase.h"
#include "AuthenticationConfig.h"
#include "GenericToken.h"
#include "Random.h"

#include "util/Buffer.h"
#include "util/Log.h"
#include "util/StringCase.h"

#include <cassert>

namespace hrb {

auto TokenDatabase::token_set(const GenericToken& token)
{
	assert(!token.owner().empty());
	assert(to_lower(token.owner()) == token.owner());
	return sanali::format(m_token_set, m_prefix, token.owner(), token.type());
}

auto TokenDatabase::token_hash(const GenericToken& token)
{
	assert(!token.owner().empty());
	assert(to_lower(token.owner()) == token.owner());
	assert(token.valid());
	return sanali::format(m_token_hash, m_prefix, token.owner(), buffer(token.data()));
}

void TokenDatabase::revoke_token(const GenericToken& session_id, Callback&& comp)
{
	constexpr char lua[] = "redis.call('ZREM', KEYS[1], ARGV[1]) redis.call('DEL', KEYS[2])";
	auto eval = sanali::CommandBuilder{"EVAL {} 2", lua}.
		append(token_set(session_id)).
		append(token_hash(session_id)).
		append(buffer(session_id.data()));
	m_redis->command(
		[comp=std::move(comp), *this](auto&& reply, auto ec) mutable
		{
			if (ec)
				Log(LOG_WARNING, "Error destroying user session key in redis: {} {}", reply, ec);
			return comp(ec);
		},
		std::move(eval)
	);
}

void TokenDatabase::revoke_all_tokens(std::string identity, TokenType type, Callback&& comp)
{
	static constexpr char lua[] = R"(
	local tokens = redis.call('ZRANGE', KEYS[1], 0, -1)
	redis.call('UNLINK', KEYS[1])
	return tokens
	)";

	to_lower(identity);
	m_redis->command(
		[identity, comp=std::move(comp), *this](auto&& reply, auto ec) mutable
		{
			if (ec || !reply.is_array())
			{
				Log(LOG_WARNING, "Error destroying all sessions for user {}: {} {}", identity, reply, ec);
				return comp(ec);
			}

			return unlink_token_hash(identity, reply, std::move(comp));
		},
		"EVAL {} 1 {}", sanali::buffer_ref(lua), token_set(GenericToken{identity, type})
	);
}

void TokenDatabase::create_token(
	TimePoint now,
	TimePoint expiry,
	const GenericToken& parent,
	const GenericToken& previous,
	TokenCallback&& comp
)
{
	assert(!parent.valid() || parent.owner()  == previous.owner());
	assert(!parent.valid() || parent.type()   == TokenType::refresh_token);
	assert(parent.valid()  == has_parent(previous.type()));
	assert(previous.type() != TokenType::none);

	auto new_token = GenericToken{previous.owner()}.
		set_type(previous.type()).
		randomize_entropy(m_session_config.entropy_length);
	assert(new_token.type() != TokenType::none);

	m_redis->command(
		[comp=std::move(comp), token=std::move(new_token), now, expiry, parent, previous, *this]
		(auto&& resp, auto ec) mutable
		{
			if (ec)
			{
				Log(
					LOG_WARNING, "Cannot increment token-sequence for identity {}: {} {} ({})",
					token.owner(), resp, ec, ec.message()
				);
				return comp(GenericToken{}, TimePoint{}, ec);
			}

			token.set_sequence_number(resp.to_int(ec));
			assert(token.valid());

			if (ec)
			{
				Log(
					LOG_WARNING, "Cannot convert token-sequence to integer {}: {} {} ({})",
					token.owner(), resp, ec, ec.message()
				);
				return comp(GenericToken{}, TimePoint{}, ec);
			}

			return store_to_database(token, previous, now, expiry, parent, std::move(comp));
		},
		"INCR {}:token-sequence:{}", m_prefix, previous.owner()
	);
}

/// Store a newly generated token into the database.
/// The functions will make sure the database is consistent after storing "new_token".
/// It will remove the oldest token of the same type if the number of tokens exceed the limit.
/// Also, if "new_token" is replacing "previous" token, this function will create a link
/// from the previous token to the new token. If such a link already exists, that means the previous
/// token has already been replaced. In that case, "new_token" is discarded and the token which replaced
/// the "previous" token is returned.
void TokenDatabase::store_to_database(
	const GenericToken& new_token,
	const GenericToken& previous,
	TimePoint now,
	TimePoint expiry,
	const GenericToken& parent,
	TokenCallback&& comp
)
{
	assert(new_token.type() != TokenType::none);
	assert(new_token.valid());
	assert(!previous.valid() || previous.type()  == new_token.type());
	assert(!previous.valid() || previous.owner() == new_token.owner());

	// Add the newly generated new_token to the sorted set.
	// The score is expiry timestamp, i.e. timestamp when new_token expires, in seconds since unix epoch.
	// The member is the new_token data, i.e. Token::data().
	// The number of tokens in the sorted set is limited. Old tokens will be rolled
	// out to make room for the new one.
	// This requires the "GT" option in the "EXPIREAT" command, which is added since redis 7.0. The redis
	// shipped with debian bookworm doesn't work.
	static constexpr const char lua[] =
	R"(
		local expiry        = ARGV[1] / 1000000000
		local expiry_ms     = math.ceil(ARGV[1] / 1000000)
		local token_data    = ARGV[2]
		local max_count     = ARGV[3]
		local parent_token  = ARGV[4]

		local token_set     = KEYS[1]
		local token_hash    = KEYS[2]
		local previous_hash = KEYS[3]

		if previous_hash ~= '' then
			local new_session = redis.call('HGET', previous_hash, 'successor')
			if type(new_session) == "string" then
				return new_session
			end
			redis.call('HSET', previous_hash, 'successor', token_data)
		end

		-- Add new token to token set, and roll out the oldest token in the set if too many
		local added = redis.call('ZADD', token_set, 'NX', expiry, token_data)
		redis.call('PEXPIREAT', token_set, expiry_ms, 'GT')

		-- Store parent in token hash
		redis.call('HSET', token_hash, 'parent', parent_token, 'expiry_ns', ARGV[1])
		redis.call('PEXPIREAT', token_hash, expiry_ms)
		return token_data
	)";

	auto next = [comp=std::move(comp), new_token=new_token, previous, now, expiry, *this](auto&& resp, auto ec) mutable
	{
		if (ec)
		{
			Log(LOG_WARNING, "Error creating new token {} in database: {} {}", new_token, resp, ec);
			return comp(GenericToken{}, TimePoint{}, ec);
		}

		// "previous" is already been replaced by another token. However, that another token has
		// already been expired or destroyed.
		if (resp.is_nil())
		{
			Log(LOG_WARNING, "Session {} already expired. Cannot refresh.", previous);
			return comp(GenericToken{}, TimePoint{}, make_error_code(std::errc::permission_denied));
		}

		new_token.set_data(resp.as_string());
		if (previous.valid() && new_token.valid())
			Log(LOG_INFO, "Refresh token {} to {}", previous, new_token);
		else if (new_token.valid())
			Log(LOG_INFO, "Created new token {}", new_token);
		else
			Log(LOG_WARNING, "Session {} is not valid even after refresh. Redis error?", new_token);

		Log(LOG_INFO, "Created new token {}", new_token);
		return housekeep_tokens(
			new_token.owner(), new_token.type(), now,
			[comp=std::move(comp), new_token, expiry](auto ec) mutable
			{
				return comp(std::move(new_token), expiry, ec);
			}
		);
	};

	auto eval = sanali::CommandBuilder{"EVAL {} 3", sanali::buffer_ref(lua)}.
		append(token_set(new_token)).       // KEYS[1]: set of tokens of the same type
		append(token_hash(new_token));      // KEYS[2]: hash to store parent token and name

	// KEYS[3]: token hash of previous token
	if (previous.valid())
		eval.append(token_hash(previous));
	else
		eval.append();

	using namespace std::chrono;

	// ARGV[1]: expiry time (nanoseconds since unix epoch)
	eval.append(expiry.time_since_epoch().count());
	eval.append(buffer(new_token.data()));          // ARGV[2]: new_token data
	eval.append(m_session_config.max_concurrent);   // ARGV[3]: max concurrent number of tokens

	// ARGV[4]: parent token
	eval.append(parent.valid() ? buffer(parent.data()) : boost::asio::const_buffer{});

	m_redis->command(std::move(next), std::move(eval));
}

void TokenDatabase::introspect(const GenericToken& token, TimePoint now, IntrospectCallback&& comp)
{
	assert(token.valid());

	static constexpr const char lua[] = R"(
		if redis.call('ZSCORE', KEYS[1], ARGV[1]) then
			return redis.call('HGET', KEYS[2], 'expiry_ns')
		end
		return nil
	)";
	sanali::CommandBuilder eval{"EVAL {} 2", sanali::buffer_ref(lua)};
	eval.append(token_set(token));
	eval.append(token_hash(token));
	eval.append(buffer(token.data()));

	auto next = [token, comp=std::move(comp), now, *this](auto&& resp, auto ec) mutable
	{
		if (ec || (!resp.is_nil() && !resp.is_string()))
		{
			Log(LOG_WARNING, "Error getting user token key in redis: {} {}", resp, ec);
			return comp(TimePoint{}, ec);
		}

		if (resp.is_nil())
		{
			// Verification failure.
			Log(LOG_DEBUG, "Session {} does not exist.", token);
			return comp(TimePoint{}, make_error_code(std::errc::permission_denied));
		}

		// The score of the token entry in the sorted set is the token start time
		TimePoint expire_in{TimePoint::duration{resp.to_int(ec)}};
		if (ec)
		{
			Log(
				LOG_WARNING, "Cannot convert token start timestamp value '{}' to integers {} ({}).",
				resp, ec, ec.message()
			);
			return comp(TimePoint{}, ec);
		}

		if (now > expire_in)
		{
			Log(LOG_DEBUG, "Token {} has already expired at {}", token, expire_in);
			return comp(expire_in, make_error_code(std::errc::permission_denied));
		}

		Log(LOG_DEBUG, "Token {} authenticated.", token);
		return comp(expire_in, ec);
	};

	m_redis->command(std::move(next), "HGET {} expiry_ns", token_hash(token));
}

void TokenDatabase::renew_session(const GenericToken& session_id, TimePoint now, TokenCallback&& comp)
{
	assert(session_id.type() == TokenType::session_id);
	auto next = [session_id, now, comp=std::move(comp), *this](auto&& refresh, auto, auto ec) mutable
	{
		if (ec)
		{
			Log(LOG_INFO, "Cannot get refresh token of {}: {} ({})", session_id, ec, ec.message());
			return comp(GenericToken{}, TimePoint{}, ec);
		}

		return refresh_token(refresh, now, session_id, std::move(comp));
	};

	// Need to introspect the refresh token before renewing this one
	return get_refresh_token(session_id, std::move(next));
}

void TokenDatabase::create_session(const std::string& identity, TimePoint now, TokenCallback&& comp)
{
	// First create a refresh token, and then use the new refresh token to create a session token
	create_refresh_token(identity, now, [*this, comp=std::move(comp), now](auto&& refresh, auto expiry, auto ec) mutable
	{
		if (ec)
			return comp(GenericToken{}, TimePoint{}, ec);

		// Make sure the expiry of the new session ID is not later than that of the refresh token.
		expiry = std::min(now + m_session_config.access_token_ttl, expiry);

		// No need to call refresh_token(), which will introspect the refresh token to make sure
		// it is valid. We can assume that the refresh token is valid because we just created it.
		create_token(now, expiry, refresh, GenericToken{refresh.owner(), TokenType::session_id}, std::move(comp));
	});
}

void TokenDatabase::verify_session(const GenericToken& session_id, TimePoint now, TokenCallback&& comp)
{
	// No need to verify invalid session
	if (!session_id.valid())
		return comp(GenericToken{}, TimePoint{}, std::make_error_code(std::errc::permission_denied));

	introspect(session_id, now, [comp=std::move(comp), now, session_id=session_id, *this](auto expiry, auto ec) mutable
	{
		if (ec)
			return comp(GenericToken{}, TimePoint{}, ec);

		if (now + m_session_config.session_id_refresh_before > expiry)
			return renew_session(session_id, now, std::move(comp));

		return comp(std::move(session_id), expiry, ec);
	});
}

void TokenDatabase::get_refresh_token(const GenericToken& token, TokenCallback&& comp)
{
	// Need to introspect the refresh token before renewing this one
	m_redis->command([comp=std::move(comp), token=token, *this](auto&& resp, auto ec) mutable
	{
		if (ec)
		{
			Log(LOG_WARNING, "Error getting refresh token from key: {} {} ({})", resp, ec, ec.message());
			return comp(GenericToken{}, TimePoint{}, ec);
		}

		if (!resp.is_string())
		{
			Log(LOG_WARNING, "{} does not have parent. {}", token, resp);
			return comp(GenericToken{}, TimePoint{}, make_error_code(std::errc::io_error));
		}

		auto refresh = GenericToken{token}.set_data(resp.as_string());
		if (refresh.type() != TokenType::refresh_token)
		{
			Log(
				LOG_WARNING, "Database error: parent of token {} is not a refresh token ({}): {}",
				token, refresh, resp
			);
			return comp(GenericToken{}, TimePoint{}, make_error_code(std::errc::io_error));
		}

		return comp(std::move(refresh), TimePoint{}, ec);
	}, "HGET {} parent", token_hash(token));
}

void TokenDatabase::create_refresh_token(std::string identity, TimePoint now, TokenCallback&& comp)
{
	auto exp = now + m_session_config.refresh_token_ttl;
	create_token(now, exp, {}, GenericToken{std::move(identity), TokenType::refresh_token}, std::move(comp));
}

void TokenDatabase::refresh_token(
	const GenericToken& refresh,
	TimePoint now,
	const GenericToken& previous,
	TokenCallback&& comp
)
{
	// Note that previous token may not be valid.
	assert(refresh.type() == TokenType::refresh_token);
	assert(previous.type() != TokenType::refresh_token);
	assert(previous.type() != TokenType::none);
	assert(previous.owner() == refresh.owner());

	// Only proceed to create a new token if the refresh token is still valid.
	introspect(refresh, now, [refresh, now, previous, comp=std::move(comp), *this] (auto expiry, auto ec) mutable
	{
		if (ec)
			return comp(GenericToken{}, TimePoint{}, ec);

		return create_token(
			// Make sure the expiry of the new session ID is not later than that of the refresh token.
			now, std::min(now + m_session_config.access_token_ttl, expiry),
			refresh, previous, [comp=std::move(comp), *this, now, previous](auto token, auto expiry, auto ec) mutable
			{
				// If we have a previous token, the "newly created token" may actually be a successor
				// that is already created before. We need to introspect it.
				if (previous.valid() && token.valid() && !ec)
					return introspect(token, now, [comp=std::move(comp), token](auto expiry, auto ec) mutable
					{
						return comp(ec ? GenericToken{} : std::move(token), expiry, ec);
					}) ;

				return comp(std::move(token), expiry, ec);
			}
		);
	});
}

void TokenDatabase::housekeep_tokens(const std::string& identity, TokenType type, TimePoint now, Callback&& comp)
{
	static constexpr char lua[] = R"(
	local token_set     = KEYS[1]
	local max_count     = ARGV[1]
	local now           = ARGV[2]

	local removed       = {};
	local excess = redis.call('ZCARD', token_set) - max_count
	if excess > 0 then
		removed = redis.call('ZPOPMIN', token_set, excess)
	end
	local expired = redis.call('ZRANGE', token_set, '-inf', now, 'BYSCORE')
	for _, v in ipairs(expired) do
		table.insert(removed, v)
	end
	redis.call('ZREMRANGEBYSCORE', token_set, '-inf', now)
	return removed
	)";
	m_redis->command([comp=std::move(comp), *this, identity](auto&& resp, auto ec) mutable
		{
			Log(LOG_INFO, "housekeep: {} {}", resp, ec);

			if (!ec && resp.is_array())
				return unlink_token_hash(identity, resp, std::move(comp));

			return comp(ec);
		},
		"EVAL {} 1 {} {} {}", sanali::buffer_ref(lua),
		token_set(GenericToken{identity, type}),
		m_session_config.max_concurrent,
		to_sec(now).time_since_epoch().count()
	);
}

void TokenDatabase::unlink_token_hash(const std::string& identity, const sanali::Resp& tokens_data, Callback&& comp)
{
	assert(tokens_data.is_array());

	// Remove the token_hash of expired tokens
	sanali::CommandBuilder unlink{"UNLINK"};
	for (auto&& token_data : tokens_data)
	{
		auto token = GenericToken{identity}.set_data(token_data.as_string());
		unlink.append(token_hash(token));
	}
	if (unlink.string_count() > 1)
		return m_redis->command([comp=std::move(comp), identity](auto&& resp, auto ec) mutable
		{
			if (ec)
				Log(LOG_WARNING, "housekeep unlink error: {} {} ({})", resp, ec, ec.message());
			else
				Log(LOG_INFO, "{}: removed {} expired tokens.", identity, resp.to_int());

			return comp(ec);
		}, std::move(unlink));
	else
		return comp(std::error_code{});
}

void TokenDatabase::get_children(const GenericToken& refresh, std::int64_t cursor, TokensBatchCallback comp)
{
	auto callback = [comp=std::move(comp), refresh, *this](auto&& resp, auto ec) mutable
	{
		std::vector<GenericToken> result;
		if (!ec && resp.array_size() == 2)
		{
			for (auto&& kv : resp[1].kv_pairs())
				result.emplace_back(refresh.owner()).set_data(kv.key());

			comp(std::move(result), ec);

			if (auto cursor = resp[0].to_int(); cursor != 0)
				get_children(refresh, cursor, comp);
		}

		// Error or end-of-iteration
		comp({}, ec);
	};

	return m_redis->command(
		std::move(callback), "ZSCAN {} {}",
		token_set(GenericToken{refresh.owner(), TokenType::session_id}),
		cursor
	);
}

void TokenDatabase::revoke_token_with_parent(
	const std::vector<GenericToken>& /*tokens*/,
	const GenericToken& parent,
	Callback&& comp
)
{
	static constexpr char lua[] = R"(
	local token_set = table.remove(KEYS, 1)
	local parent    = table.remove(ARGV, 1)
	for index, token_hash in ipairs(KEYS) do
		if redis.call('HGET', token_hash, 'parent') == parent then
			redis.call('UNLINK', token_hash)
			redis.call('ZREM', token_set, ARGV[index])
		end
	end
	)";
	return m_redis->command([comp=std::move(comp)](auto&&,auto ec)mutable{comp(ec);}, "EVAL {} 1 {}", lua, parent);
}

} // end of namespace hrb
