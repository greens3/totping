/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 9/1/23.
//

#pragma once

#include <fmt/format.h>

#include <boost/asio/any_completion_handler.hpp>

#include "Sanali.hh"

#include <chrono>
#include <cstdint>
#include <filesystem>
#include <string_view>

namespace sanali {
class CommandBuilder;
}

namespace hrb {

struct AuthenticationConfig;
class GenericToken;
enum class TokenType;
class ConstBuffer;

class TokenDatabase
{
public:
	using TimePoint = std::chrono::system_clock::time_point;
	using TimePointSeconds = std::chrono::time_point<
	    std::chrono::system_clock,
		std::chrono::duration<double, std::chrono::seconds::period>
    >;
	static auto to_sec(TimePoint tp) {return std::chrono::time_point_cast<TimePointSeconds::duration>(tp);}

	using TokenCallback = boost::asio::any_completion_handler<void(const GenericToken&, TimePoint expiry, std::error_code)>;
	using IntrospectCallback = boost::asio::any_completion_handler<void(TimePoint expiry, std::error_code)>;
	using Callback = boost::asio::any_completion_handler<void(std::error_code)>;
	using TokensBatchCallback = std::function<void(std::vector<GenericToken>&&, std::error_code)>;

public:
	TokenDatabase(
		sanali::Redis redis, std::string prefix,
		const AuthenticationConfig& session_config
	) :
		m_prefix{std::move(prefix)}, m_session_config{session_config},
		m_redis{std::move(redis)}
	{
	}

	void create_refresh_token(std::string identity, TimePoint now, TokenCallback&& comp);
	void refresh_token(const GenericToken& refresh, TimePoint now, const GenericToken& previous, TokenCallback&& comp);

	void revoke_token(const GenericToken& token, Callback&& comp);
	void revoke_all_tokens(std::string identity, TokenType type, Callback&& comp);

	void introspect(const GenericToken& token, TimePoint now, IntrospectCallback&& comp);
	void get_refresh_token(const GenericToken& token, TokenCallback&& comp);
	void renew_session(const GenericToken& session_id, TimePoint now, TokenCallback&& comp);

	// Helpers for session management with auto-renew
	void create_session(const std::string& identity, TimePoint now, TokenCallback&& comp);
	void verify_session(const GenericToken& session_id, TimePoint now, TokenCallback&& comp);

	void housekeep_tokens(const std::string& identity, TokenType type, TimePoint now, Callback&& comp);

	void get_children(const GenericToken& refresh, std::int64_t cursor, TokensBatchCallback comp);

private:
	void create_token(
		TimePoint now,
		TimePoint expiry,
		const GenericToken& parent,
		const GenericToken& previous,
		TokenCallback&& comp
	);
	void store_to_database(
		const GenericToken& new_token,
		const GenericToken& previous,
		TimePoint now,
		TimePoint expiry,
		const GenericToken& parent,
		TokenCallback&& comp
	);

	// Database keys schema
	static constexpr const char m_token_set[]  = "{}:tokens:{}:{}";
	static constexpr const char m_token_hash[] = "{}:token:{}:{}";
	std::size_t data_position_in_hash_key(std::string_view identity) const
	{
		return m_prefix.size() + identity.size() + 3;
	}

	auto token_set(const GenericToken& token);
	auto token_hash(const GenericToken& token);

	void unlink_token_hash(const std::string& identity, const sanali::Resp& tokens_data, Callback&& comp);
	void revoke_token_with_parent(const std::vector<GenericToken>& tokens, const GenericToken& parent, Callback&& comp);

private:
	std::string             m_prefix;
	const AuthenticationConfig&    m_session_config;
	sanali::Redis           m_redis;
};

} // end of namespace hrb
