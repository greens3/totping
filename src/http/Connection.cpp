/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 2/17/18.
//

#include "Connection.h"

#include <boost/asio/dispatch.hpp>

#include "util/Log.h"

namespace hrb {

Connection::Connection(boost::asio::ip::tcp::socket socket, RequestHandler handler) :
	m_stream{std::move(socket)},
	m_handler{std::move(handler)}
{
}

void Connection::run()
{
	m_stream.expires_after(std::chrono::seconds{30});
	boost::asio::dispatch(m_stream.get_executor(), [self=shared_from_this()]{self->do_read();});
}

void Connection::do_read()
{
	m_request = {};

	async_read(m_stream, m_buffer, m_request,
		[self=shared_from_this()](auto ec, auto bytes) {self->on_read(ec, bytes);}
	);
}

void Connection::on_read(boost::beast::error_code ec, std::size_t)
{
    if (ec == http::error::end_of_stream)
        return do_close();

    auto sender = [this, self=shared_from_this()](auto&& response)
    {
        // The lifetime of the message has to extend
        // for the duration of the async operation, so
        // we use a shared_ptr to manage it.
        using Response = std::remove_reference_t<decltype(response)>;
        auto sp = std::make_shared<Response>(std::forward<Response>(response));
//		sp->set(http::field::server, version);
		sp->keep_alive(m_request.keep_alive());
        sp->prepare_payload();

        async_write(m_stream, *sp,
            [this, self=shared_from_this(), sp](auto&& ec, auto)
            {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                if (sp->need_eof())
                    return do_close();

                // Read another request
                if (!ec)
                    do_read();
            }
        );
    };

    if (ec)
    {
        Log(LOG_WARNING, "Error reading HTTP request from peer: {} ({})", ec, ec.message());
    }
    else
    {
        // Handle the request
        m_handler(std::move(m_request), std::move(sender));
    }
}

void Connection::do_close()
{
	// Send a TCP shutdown
	boost::beast::error_code ec;
	m_stream.socket().shutdown(tcp::socket::shutdown_send, ec);

	// At this point the connection is closed gracefully
}

} // end of namespace hrb
