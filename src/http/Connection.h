/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 2/17/18.
//

#include "HTTP.h"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>

#include <memory>

#pragma once

namespace libconfig {class Config;}

namespace hrb {

class Connection : public std::enable_shared_from_this<Connection>
{
public:
	explicit Connection(boost::asio::ip::tcp::socket socket, RequestHandler handler);

	void run();

private:
	void do_read();
	void on_read(boost::beast::error_code ec, std::size_t);
	void do_close();

private:
	boost::beast::tcp_stream    m_stream;
	boost::beast::flat_buffer   m_buffer;

	RequestHandler              m_handler;

	http::request<http::string_body>    m_request;
};

} // end of namespace hrb
