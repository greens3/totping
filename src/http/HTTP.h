//
// Created by nestal on 2/17/24.
//

#pragma once

#include "SplitBuffers.h"

#include <boost/asio/any_io_executor.hpp>
#include <boost/asio/any_completion_handler.hpp>

#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/core.hpp>

#include <functional>

// Some shortcuts
namespace hrb {

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>

constexpr std::size_t template_count = 3;

using ResponseSender = boost::asio::any_completion_handler<
    void(http::response<http::empty_body>&&),
	void(http::response<SplitBuffers<template_count>>&&)
>;

using RequestHandler = std::function<void(
	http::request<http::string_body>&&,
	ResponseSender
)>;

using RequestHandlerFactory = std::function<RequestHandler(boost::asio::any_io_executor exec)>;

}