/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/1/18.
//

#include "Listener.h"
#include "Connection.h"

#include "util/Log.h"

#include <boost/asio/strand.hpp>
#include <fmt/std.h>

namespace hrb {

Listener::Listener(
	boost::asio::any_io_executor ioc,
	RequestHandlerFactory handler,
	const boost::asio::ip::tcp::endpoint& endpoint
) :
	m_ioc{std::move(ioc)},
	m_acceptor{boost::asio::make_strand(m_ioc)},
	m_handler{std::move(handler)}
{
	// Open the acceptor
	boost::system::error_code ec;
	m_acceptor.open(endpoint.protocol(), ec);
	if (ec)
		throw std::system_error(ec);

	m_acceptor.set_option(boost::asio::socket_base::reuse_address{true});

	// Bind to the server address
	m_acceptor.bind(endpoint, ec);
	if (ec)
		throw std::system_error(ec);

	// Start listening for connections
	Log(LOG_INFO, "Listening to {}:{}", endpoint.address().to_string(), endpoint.port());
	m_acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
	if (ec)
		throw std::system_error(ec);
}

void Listener::run()
{
	if (m_acceptor.is_open())
		do_accept();
}

void Listener::do_accept()
{
	m_acceptor.async_accept(
		boost::asio::make_strand(m_ioc),
		[self = this->shared_from_this()](auto ec, boost::asio::ip::tcp::socket socket)
		{
			self->on_accept(ec, std::move(socket));
		}
	);
}

void Listener::on_accept(boost::system::error_code ec, boost::asio::ip::tcp::socket socket)
{
	if (ec)
	{
		Log(LOG_WARNING, "accept error: {}", ec);
	}
	else
	{
		std::make_shared<Connection>(std::move(socket), m_handler(socket.get_executor()))->run();
	}

	// Accept another connection
	do_accept();
}

} // end of namespace
