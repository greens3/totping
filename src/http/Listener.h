/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/1/18.
//

#pragma once

#include "HTTP.h"

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/any_io_executor.hpp>

#include <optional>
#include <memory>
#include <functional>

namespace hrb {

// Accepts incoming connections and launches the sessions
class Listener : public std::enable_shared_from_this<Listener>
{
public:
	Listener(boost::asio::any_io_executor ioc, RequestHandlerFactory handler, const boost::asio::ip::tcp::endpoint& endpoint);

	void run();

private:
	void do_accept();

	void on_accept(boost::system::error_code ec, boost::asio::ip::tcp::socket socket);

private:
	boost::asio::any_io_executor m_ioc;
	boost::asio::ip::tcp::acceptor m_acceptor;

	RequestHandlerFactory m_handler;
};

} // end of hrb namespace
