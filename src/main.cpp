#include "Azuma.h"
#include "crypt/Password.h"
#include "crypt/TOTP.h"
#include "crypt/TokenDatabase.h"
#include "crypt/IdentityDatabase.h"
#include "crypt/qrcodegen.hpp"
#include "http/Listener.h"
#include "util/Log.h"
#include "util/RenderQrCode.h"
#include "Version.h"

#include <yaml-cpp/yaml.h>

#include <fmt/format.h>

#include <boost/asio/thread_pool.hpp>
#include <boost/algorithm/hex.hpp>

#include <cstdlib>
#include <iostream>
#include <memory>

// getopt()
#include <unistd.h>
#include <termios.h>

using namespace hrb;

struct DisableEcho
{
	explicit DisableEcho(int fd = STDIN_FILENO) : m_fd{fd}
	{
		// Disable echo
		::tcgetattr(m_fd, &tty);
		tty.c_lflag &= ~ECHO;
        ::tcsetattr(m_fd, TCSANOW, &tty);
	}
	~DisableEcho()
	{
		tty.c_lflag |= ECHO;
        ::tcsetattr(m_fd, TCSANOW, &tty);
	}

	int m_fd{-1};
	struct termios tty{};
};

void add_user(IdentityDatabase db, std::string username, std::string issuer)
{
	fmt::print("Please type password for user {}:\n", username);

	// Disable echo before reading password from stdin
	DisableEcho disable_echo;
	std::string password;
	if (getline(std::cin, password))
	{
		TOTP totp{TOTP::randomize_secret()};
		db.add_identity(
			username, Password{std::move(password)}, totp,
			[username, totp, issuer = std::move(issuer)](auto ec) {
				// Unix programs print nothing when success
				if (ec)
					fmt::print(stderr, "{}\n", ec.message());
				else
				{
					assert(totp.valid());
					fmt::print("{}\n", render_qrcode(totp.generate_uri(username, issuer)));
				}
			}
		);
	}
}

void del_user(IdentityDatabase db, const std::string& username)
{
	db.remove_identity(
		username, [](auto ec) {
			// Unix programs print nothing when success
			if (ec)
				fmt::print(stderr, "{}\n", ec.message());
		}
	);
}

void print_totp_token(IdentityDatabase db, const std::string& username)
{
	db.get_totp(username, [](auto&& totp, auto ec)
	{
		if (!ec)
		{
			if (totp.valid())
				fmt::print("{}\n", totp.auth_token());
			else
				fmt::print("Error! invalid TOTP secret in database.\n");
		}
		else
			fmt::print(stderr, "{}\n", ec.message());
	});
}

void print_totp_secret(IdentityDatabase db, const std::string& username, std::string issuer, bool hex)
{
	db.get_totp(username, [username, hex, issuer=std::move(issuer)](auto&& totp, auto ec)
	{
		if (!ec)
		{
			if (totp.valid())
			{
				std::string result;
				if (hex)
					boost::algorithm::hex(totp.secret(), std::back_inserter(result));
				else
					result = render_qrcode(totp.generate_uri(username, issuer));
				fmt::print("{}\n", result);
			}
			else
				fmt::print("Error! invalid TOTP secret in database.\n");
		}
		else
			fmt::print(stderr, "{}\n", ec.message());
	});
}

std::string default_config_filename(const char* argv0)
{
	assert(argv0);
	if (auto env = std::getenv("AZUMA_CONF"); env && std::strlen(env) > 0)
		return env;

	return std::string{::basename(argv0)}.append(".yml");
}

int main(int argc, char** argv)
{
	auto config_file{default_config_filename(argv[0])};
	std::string username;
	enum class Command {add_user, del_user, totp_token, totp_secret_qr, totp_secret_hex, none} command = Command::none;

	int opt;
	while ((opt = ::getopt(argc, argv, "c:u:adtqh")) != -1)
	{
		switch (opt)
		{
		case 'c':
			config_file.assign(::optarg);
			break;
		case 'u':
			username.assign(::optarg);
			break;
		case 'a':
			command = Command::add_user;
			break;
		case 'd':
			command = Command::del_user;
			break;
		case 't':
			command = Command::totp_token;
			break;
		case 'q':
			command = Command::totp_secret_qr;
			break;
		case 'h':
			command = Command::totp_secret_hex;
			break;
		default:
			fmt::print(stderr, "Usage: {} [-a user] [-r user] [-c config]\n", argv[0]);
			std::exit(EXIT_FAILURE);
		}
	}

	Configuration configuration{YAML::LoadFile(config_file)};
	if (command != Command::none)
	{
		boost::asio::io_context ioc;
		sanali::RedisPool redis{configuration.redis_host(), configuration.redis_port()};

		redis.alloc(ioc.get_executor(), [&, thread=std::this_thread::get_id()](auto redis, auto ec)
		{
			if (ec)
			{
				fmt::print(stderr, "Cannot connect to redis {} ({})\n", ec, ec.message());
				return;
			}

			assert(std::this_thread::get_id() == thread);
			SharedStatistic dont_care;
			Azuma azuma{std::move(redis), configuration, dont_care};

			switch (command)
			{
				using enum Command;
			case add_user:  ::add_user(azuma.identity_db(), username, configuration.database_name());    break;
			case del_user:  ::del_user(azuma.identity_db(), username);                                   break;
			case totp_token:
				print_totp_token(azuma.identity_db(), username);
				break;
			case totp_secret_qr:
			case totp_secret_hex:
				print_totp_secret(azuma.identity_db(), username, configuration.database_name(), command == totp_secret_hex);
				break;
			case none:
			default:
				assert(false);
				break;
			}
		});

		ioc.run();
	}
	else
	{
		boost::asio::thread_pool exec;
		sanali::RedisPool redis{configuration.redis_host(), configuration.redis_port()};
		SharedStatistic stat;

		auto handler = [&configuration, &redis, &stat](auto sock_exec) mutable
		{
			// "sock_exec" is the executor of the TCP socket of the current HTTP connection.
			// We must use this executor to run the callbacks of sanali::Redis, otherwise there will be
			// multi-thread race conditions.
			std::error_code ec;
			return Azuma{redis.alloc(std::move(sock_exec), ec), configuration, stat};
		};

		Log(LOG_INFO,
			"Azuma version {} database '{}' starting",
			version(),
			configuration.database_name()
		);

		std::make_shared<Listener>(exec.get_executor(), handler, configuration.listen_addr())->run();

		exec.attach();
	}
    return 0;
}
