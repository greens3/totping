//
// Created by root on 3/26/24.
//

#pragma once

#include <boost/asio/buffer.hpp>
#include <compare>
#include <span>

namespace hrb {
using boost::asio::buffer;

// Simple wrapper around boost::asio::const_buffer
class ConstBuffer
{
public:
	ConstBuffer() = default;
	ConstBuffer(const void* data, std::size_t size) : m_buf{data, size} {}

	// Accept anything that can be converted to boost::asio::const_buffer via boost::asio::buffer().
	// Those include std::string, vector<char/unsigned char>, std::array etc.
	template <typename T>
	requires requires(T t) {{boost::asio::buffer(t)} -> std::convertible_to<boost::asio::const_buffer>;}
	explicit ConstBuffer(T&& t) : m_buf{boost::asio::buffer(t)}
	{
	}

	// Models boost::asio::const_buffer
	auto data()     const {return m_buf.data();}
	auto size()     const {return m_buf.size();}

	// Models container of chars
	auto char_data()    const {return static_cast<const char*>(m_buf.data());}
	auto string_view()  const {return std::string_view{char_data(), size()};}
	auto char_begin()   const {return string_view().begin();};
	auto char_end()     const {return char_begin() + m_buf.size();};

	// Models container of uint8_t (aka unsigned char)
	auto u8data()   const {return static_cast<const std::uint8_t*>(data());}
	auto span() const
	{
		return std::span<const std::uint8_t>{u8data(), size()};
	}
	auto begin() const {return span().begin();}
	auto end()   const {return span().end();}
	auto& buffer()      const {return m_buf;}

	// Deep copy!
	auto to_string()    const {return std::string{string_view()};}
	auto to_vec()       const {return std::vector<std::uint8_t>(begin(), end());}

	auto operator<=>(const ConstBuffer& that) const
	{
		return std::lexicographical_compare_three_way(begin(), end(), that.begin(), that.end());
	}

	bool operator==(const ConstBuffer& that) const
	{
		return this->size() == that.size() && std::memcmp(this->data(), that.data(), size()) == 0;
	}

private:
	boost::asio::const_buffer m_buf;
};

}
