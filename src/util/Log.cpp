//
// Created by nestal on 2/24/24.
//

#include "Log.h"

namespace hrb::detail {
namespace {
bool is_stdout_tty()
{
	static const bool result = ::isatty(STDOUT_FILENO);
	return result;
}
} // end of local namespace

void log_line(int priority, fmt::memory_buffer&& line)
{
	// Write to stdout if it is a terminal, otherwise use syslog()
	// See https://www.freedesktop.org/software/systemd/man/systemd.exec.html#StandardOutput=
	if (!is_stdout_tty())
	{
		// See the printf() man page for details about _precision_
		// https://man7.org/linux/man-pages/man3/printf.3.html
		::syslog(
			priority, "%.*s",
			static_cast<int>(line.size()),
			line.data()
		);
	}
	else
	{
		line.push_back('\n');
		::write(STDOUT_FILENO, line.data(), line.size());
	}
}

}