//
// Created by nestal on 2/17/24.
//

#pragma once

#include <fmt/ostream.h>
#include <fmt/std.h>
#include <fmt/format.h>
#include <fmt/chrono.h>

#include <boost/system/error_code.hpp>
#include <boost/beast/core/string.hpp>

#include <filesystem>
#include <source_location>
#include <string_view>

#include <syslog.h>

namespace hrb {
namespace detail {
void log_line(int priority, fmt::memory_buffer&& line);
} // end of namespace detail

template <typename... Args>
struct Log
{
    Log(
        int priority,
        fmt::format_string<Args...> format_string,
        Args && ... args,
        const std::source_location& location = std::source_location::current()
    )
    {
        fmt::memory_buffer line;
        fmt::format_to(std::back_inserter(line), format_string, std::forward<Args>(args)...);
        fmt::format_to(
            std::back_inserter(line),
            " ({}:{})",

            // only the filename part is needed
            std::filesystem::path{location.file_name()}.filename().string(),
            location.line()
        );

        detail::log_line(priority, std::move(line));
    }
};

template <typename... Args>
Log(int, fmt::format_string<Args...>, Args && ...) -> Log<Args...>;

}

namespace boost::system {
inline std::error_code format_as(const error_code& ec)
{
	return std::error_code{ec.value(), ec.category()};
}
} // end of namespace boost::system

namespace boost::core {
inline std::string_view format_as(const string_view& s)
{
	return {s.data(), s.size()};
}
} // end of namespace boost::core