//
// Created by nestal on 3/10/24.
//

#include "RenderQrCode.h"

namespace hrb {

std::string render_qrcode(const qrcodegen::QrCode& qr, std::size_t border)
{
	// One vertical line has two units of border.
	std::string result((border+1)/2, '\n');

	enum Block {full, upper, lower, empty};
	auto set_lower_half = [](Block block)
	{
		return  block == upper ? full : (
				block == empty ? lower : empty
		);
	};

	for (int y = 0; y < qr.getSize(); y+=2)
	{
		std::vector<Block> line;
		line.reserve(qr.getSize());

		for (int x = 0; x < qr.getSize(); x++)
			line.push_back(qr.getModule(x, y) ? upper : empty);

		assert(line.size() == static_cast<unsigned>(qr.getSize()));

		// Next line, if any
		for (int x = 0; y+1 < qr.getSize() && x < qr.getSize(); x++)
		{
			if (qr.getModule(x, y+1))
				line[x] = set_lower_half(line[x]);
		}

		result.append(border, ' ');

		// translate the line into UTF-8
		constexpr std::string_view blocks[] = {"█", "▀", "▄", " "};
		for (auto block : line)
			result.append(blocks[block]);
		result.append("\n");
	}
	return result.append((border+1)/2, '\n');
}

std::string render_qrcode(const std::string& text, std::size_t border)
{
	return render_qrcode(qrcodegen::QrCode::encodeText(text.c_str(), qrcodegen::QrCode::Ecc::MEDIUM), border);
}

} // end of namespace hrb
