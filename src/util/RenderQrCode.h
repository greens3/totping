//
// Created by nestal on 3/10/24.
//

#pragma once

#include "crypt/qrcodegen.hpp"
#include <string>

namespace hrb {

std::string render_qrcode(const qrcodegen::QrCode& qr, std::size_t border=2);
std::string render_qrcode(const std::string& text, std::size_t border=2);

}