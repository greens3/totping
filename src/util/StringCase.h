//
// Created by nestal on 3/2/24.
//

#pragma once

#include <ranges>
#include <utility>
#include <algorithm>
#include <fmt/format.h>

namespace hrb {

template <typename CharRange>
requires
	std::ranges::input_range<CharRange> &&
    std::ranges::output_range<CharRange, std::ranges::range_value_t<CharRange>>
auto& to_lower(CharRange& inout, const std::locale& loc = std::locale{}
)
{
	std::transform(
		std::ranges::begin(inout), std::ranges::end(inout), std::ranges::begin(inout),
		[&loc](std::ranges::range_value_t<CharRange> c){return std::tolower(c, loc);}
	);
	return inout;
}

// Do not bind to l-value references. Use the above overload for l-value references.
template <typename CharRange>
requires
	(!std::is_lvalue_reference_v<CharRange>) &&
	std::ranges::input_range<CharRange> &&
    std::ranges::output_range<CharRange, std::ranges::range_value_t<CharRange>>
auto&& to_lower(CharRange&& inout, const std::locale& loc = std::locale{})
{
	// inout is an l-value reference now, so it's not recursive call
	to_lower(inout, loc);
	return std::move(inout);
}

template <typename CharRange>
requires std::ranges::input_range<CharRange>
auto to_lower_copy(const CharRange& inout, const std::locale& loc = std::locale{})
{
	using StringType = std::basic_string<std::ranges::range_value_t<CharRange>>;
	return to_lower(StringType{std::ranges::begin(inout), std::ranges::end(inout)}, loc);
}

template <typename CharT>
inline auto to_lower_copy(const CharT* str)
{
	return to_lower(std::basic_string<CharT>(str));
}

} // end of namespace
