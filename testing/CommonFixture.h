//
// Created by nestal on 2/18/24.
//

#pragma once

#include "Sanali.hh"
#include <cstdlib>

namespace hrb::test {

class CommonFixture
{
public:
	CommonFixture() = default;

protected:
	bool RunFor(bool& cond, std::chrono::seconds duration = std::chrono::seconds{10})
	{
		auto until = std::chrono::steady_clock::now() + duration;
		while (!cond && ioc.run_one_until(until));
		ioc.restart();
		return std::exchange(cond, false);
	}

	sanali::Redis connect_redis()
	{
		const auto* env_host = std::getenv("HRB_REDIS_HOST");
		if (!env_host)
			env_host = "localhost";

		const auto* env_port = std::getenv("HRB_REDIS_PORT");
		if (!env_port)
			env_port = "6379";

		auto conn = std::make_shared<sanali::Connection>(ioc.get_executor());

		std::error_code ec;
		conn->connect(env_host, env_port, ec);
		if (ec)
			throw std::system_error(ec);

		return sanali::Redis{std::move(conn)};
	}

protected:
	boost::asio::io_context ioc;
};

}