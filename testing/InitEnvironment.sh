#!/bin/bash

# Typically run in "testing" directory as:
# AZUMA_CONF=../azuma.yml PATH=$PATH:../cmake-build-debug/src ./InitEnvironment.sh user3 password

username="$1"
password="$2"

read -r -d '' format <<'EOF'
{
  "dev": {
    "name": "%s",
    "password": "%s",
    "totp_secret": "%s"
  }
}
EOF

# add user
echo "$password" | azuma -u "${username}" -a
totp_secret=$(azuma -u "${username}" -h)

printf "${format}\n" "$username" "$password" "$totp_secret" > http-client.private.env.json
