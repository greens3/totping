//
// Created by nestal on 3/10/24.
//

#include <catch2/catch_test_macros.hpp>

#include "util/RenderQrCode.h"

#include <iostream>
using namespace hrb;

TEST_CASE("Test render QR code", "[normal]")
{
	auto qr = qrcodegen::QrCode::encodeText("Hello world", qrcodegen::QrCode::Ecc::MEDIUM);
	std::string result = render_qrcode(qr, 0);

	REQUIRE(result == "█▀▀▀▀▀█ ▀██ ▄ █▀▀▀▀▀█\n"
	                  "█ ███ █  █ █▀ █ ███ █\n"
	                  "█ ▀▀▀ █ █▀  █ █ ▀▀▀ █\n"
	                  "▀▀▀▀▀▀▀ ▀▄█▄█ ▀▀▀▀▀▀▀\n"
	                  " ██▀▀▀▀█▄   ▀▄▄██▄▄ █\n"
	                  "▄▀▀   ▀▄█▄▀▄██▀ ▄██▀ \n"
	                  "  ▀▀▀▀▀ ██ ▄█▀▀ ▄▄  ▀\n"
	                  "█▀▀▀▀▀█ █▄▀ █▄▀▄ ▄██▄\n"
	                  "█ ███ █ █▀█ █▄██▄▄  ▀\n"
	                  "█ ▀▀▀ █ █ █ █▄ ▀▄▄█  \n"
	                  "▀▀▀▀▀▀▀  ▀ ▀▀ ▀ ▀  ▀ \n");
}
