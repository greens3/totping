//
// Created by root on 3/27/24.
//

#include <catch2/catch_test_macros.hpp>

#include "crypt/GenericToken.h"
#include "SessionManagement.h"
#include "http/Cookie.h"
#include "http/HTTP.h"

#include <cppcodec/base64_rfc4648.hpp>

using namespace hrb;
using namespace std::chrono_literals;

using base64 = cppcodec::base64_rfc4648;

TEST_CASE("Test cookie session", "[normal]")
{
	constexpr std::size_t key_length{21};
	auto session = GenericToken{"sumsum", TokenType::session_id}.randomize_entropy(key_length);

	http::header<true, http::fields> header;
	header.set(http::field::cookie, set_cookie(session, 100s).str());

	REQUIRE(extract_session_id(header) == session);

	auto access_token = session.set_type(TokenType::access_token).to_string();

	http::header<true, http::fields> header2;
	header2.set(http::field::authorization, "Basic " + ConstBuffer{base64::encode(access_token)}.to_string());
	REQUIRE(extract_session_id(header2) == GenericToken{session}.set_type(TokenType::access_token));

	REQUIRE(extract_session_id({}) == GenericToken{});
}
