#!/bin/bash

username="$1"
password="$2"
totp=$(./azuma -u "${username}" -t)

# Try login
curl -c cookie -v -d "username=${username}&password=${password}&totp=${totp}" http://localhost:8964/login

# Use the cookie to authenticate
curl -b cookie -v http://localhost:8964/auth

# Logout
curl -b cookie -v http://localhost:8964/logout

# Use the cookie to authenticate, will fail now
curl -b cookie -v http://localhost:8964/auth
