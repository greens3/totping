//
// Created by nestal on 2/18/24.
//

#include <catch2/catch_test_macros.hpp>

#include <cppcodec/base64_rfc4648.hpp>
#include <cppcodec/base32_rfc4648.hpp>

#include "CommonFixture.h"

#include "crypt/TokenDatabase.h"
#include "crypt/IdentityDatabase.h"
#include "crypt/GenericToken.h"
#include "crypt/AuthenticationConfig.h"
#include "crypt/Random.h"
#include "util/StringCase.h"

#include <fmt/chrono.h>
#include <fmt/os.h>
#include <fmt/ostream.h>

#include <ranges>

using namespace hrb;
using namespace hrb::test;
using namespace std::chrono_literals;

using base64 = cppcodec::base64_rfc4648;
using base32 = cppcodec::base32_rfc4648;

TEST_CASE("Test random", "[normal]")
{
	std::uint64_t num{};
	insecure_random(num);
	REQUIRE(num != 0);
	REQUIRE(insecure_random<int>() != 0);

	struct {std::uint64_t a[4];} big_num{};
	REQUIRE(big_num.a[3] == 0);
	REQUIRE(sizeof(big_num) > sizeof(num));
	insecure_random(big_num);
	REQUIRE(big_num.a[0] != 0);
	REQUIRE(big_num.a[1] != 0);
	REQUIRE(big_num.a[2] != 0);
	REQUIRE(big_num.a[3] != 0);

	auto num2 = insecure_random<std::uint64_t>();
	REQUIRE(num2 != 0);

	auto str = insecure_random(std::string(10, {}));
	REQUIRE(str.size() == 10);

	auto arr = insecure_random(std::array<std::uint8_t, 20>{});
	REQUIRE(arr.size() == 20);
	REQUIRE(arr != std::array<std::uint8_t, 20>{});
}

TEST_CASE("Base64 simple tests", "[normal]")
{
	std::vector<std::uint8_t> in{0, 1, 2, 3, 4, 5, 6, 7};
	REQUIRE(base64::encode(in.data(), in.size()) == "AAECAwQFBgc=");
	REQUIRE(base64::decode("AAECAwQFBgc=") == in);

	auto large = insecure_random(std::vector<std::uint8_t>(5000));
	REQUIRE(base64::decode(base64::encode(large.data(), large.size())) == large);
}

TEST_CASE("Base32 simple test", "[normal]")
{
	std::vector<std::uint8_t> in{0, 1, 2, 3, 4, 5, 6, 7};
	auto s = base32::encode(in.data(), in.size());
	REQUIRE(s == "AAAQEAYEAUDAO===");
    REQUIRE(base32::decode(s) == in);
}

TEST_CASE("Token roundtrip", "[normal]")
{
	// Should work even for owner has colon
	auto subject = GenericToken{"some:User"}.
		set_type(TokenType::session_id).
		set_sequence_number(0x10FFFF).randomize_entropy(24);
	auto copy{GenericToken{}.from_string(subject.to_string())};
	REQUIRE(copy == subject);

	// Also verify operator==
	REQUIRE(copy.owner() == "some:user");
	REQUIRE(copy.entropy() == subject.entropy());
	REQUIRE(copy.entropy().size() == 24);
	REQUIRE(copy.sequence_number() == 0x10FFFF);

	auto from_data = GenericToken{copy.owner()}.set_type(TokenType::session_id).set_data(copy.data());
	REQUIRE(from_data.sequence_number() == 0x10FFFF);

	// Invalid session IDs
	auto invalid = GenericToken{copy}.set_data();
	REQUIRE_FALSE(invalid.valid());
	REQUIRE(invalid.owner() == "some:user");
	REQUIRE(invalid.entropy_short() == "");

	// Clear the entropy
	auto invalid2 = GenericToken{copy}.set_sequence_number(101).set_entropy({});
	REQUIRE_FALSE(invalid2.valid());
	REQUIRE(invalid2.owner() == "some:user");
	REQUIRE(invalid2.sequence_number() == 101);
	REQUIRE(invalid2.entropy_short() == "");
}

TEST_CASE("Case conversion with range", "[normal]")
{
	std::string mixed{"AbCdEf"};
	REQUIRE(to_lower(mixed) == "abcdef");
	REQUIRE(mixed == "abcdef");

	char mixed2[] = "ABCdef";
	to_lower(mixed2);
	REQUIRE(mixed2 == std::string{"abcdef"});

	// r-value references
	auto lower = to_lower(std::string{"OOOkkk"});
	REQUIRE(lower == "oookkk");

	// char array
	auto lower2 = to_lower_copy("WWWmmm");
	REQUIRE(lower2 == "wwwmmm");

	// const std::string: converted to std::string_view
	const std::string clower{"LOWERCASE"};
	REQUIRE(to_lower_copy(clower) == "lowercase");
	REQUIRE(clower == "LOWERCASE");
	static_assert(std::is_same_v<decltype(to_lower_copy(clower)), std::string>);

	// const char array
	const char carray[] = "LOWerCAse";
	REQUIRE(to_lower_copy(carray) == "lowercase");

	// For long string (i.e. longer than SSO), to_lower(string&&) should not copy the string
	std::string long_string{"A very loOOOOOOOOOOOOOOOOOOOOOOOOOOOOooooooong string"};
	auto data_address = long_string.data();
	auto long_output = to_lower(std::move(long_string));
	REQUIRE(long_output ==  "a very loooooooooooooooooooooooooooooooooooong string");
	REQUIRE(long_output.data() == data_address);
}

TEST_CASE_METHOD(CommonFixture, "Add user and login", "[normal]" )
{
	auto redis = connect_redis();

	bool checked{};
	const TOTPConfig totp_config{};

	// Use a randomized database name to avoid timing issues from previous runs
	// It is possible to run into three-strike rejections if we run the UT too frequently
	auto db_name = std::string{"azuma"} + cppcodec::base64_rfc4648::encode(insecure_random(std::array<std::uint8_t, 5>{}));
	INFO("Set database name to " << db_name);

	std::chrono::seconds session_length{3600};
	AuthenticationConfig session_config{
		.refresh_token_ttl=session_length, .access_token_ttl=session_length
	};
	REQUIRE(session_config.session_id_refresh_before == 2700s);
	const auto refresh_after = session_config.refresh_token_ttl - session_config.session_id_refresh_before;

	IdentityDatabase id_db{redis, db_name, session_config, totp_config};
	TokenDatabase token_db{redis, db_name, session_config};

	TOTP rand_totp{TOTP::randomize_secret()};
	// Invalid owner
	id_db.add_identity(
		"name with space", Password{"aaa"}, rand_totp, [&checked](auto ec) {
			REQUIRE(ec == std::errc::invalid_argument);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
	id_db.add_identity(
		"nameWith:colon", Password{"aaa"}, rand_totp, [&checked](auto ec) {
			REQUIRE(ec == std::errc::invalid_argument);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Use randomized password for testing
	Password sum_password{insecure_random(std::string(20, {}))};

	// Add user "sumsum"
	id_db.add_identity("sumsum", sum_password, rand_totp, [&checked](auto ec)
	{
		REQUIRE_FALSE(ec);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	// Get TOTP for user
	id_db.get_totp("sumsum", [&checked, rand_totp](auto&& totp, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(totp == rand_totp);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	auto now  = std::chrono::system_clock::now();

	// Wrong password will be rejected even with correct TOTP token
	id_db.authenticate(
		{"SUMsum", Password{insecure_random(std::string(19, {}))}, rand_totp.auth_token(now)}, now,
		[&checked](auto ec)
		{
			REQUIRE(ec == std::errc::permission_denied);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Even with a correct password, login will be rejected if it's too close the last failure.
	// Need to wait at least one second.
	id_db.authenticate(
		{"sumsum", sum_password, rand_totp.auth_token(now)}, now + 500ms,
		[&checked](auto ec)
		{
			REQUIRE(ec == std::errc::device_or_resource_busy);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Even with an incorrect password, login will be rejected and result will be EBUSY instead of
	// EACCES. The password is not validated anyway.
	id_db.authenticate(
		{"sumsum", Password{insecure_random(std::string(19, {}))}, rand_totp.auth_token(now)},
		now + 600ms,
		[&checked](auto ec)
		{
			REQUIRE(ec == std::errc::device_or_resource_busy);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Login with correct password and TOTP token
	auto session_start = now+31s;
	id_db.authenticate(
		{"SUMsum", sum_password, rand_totp.auth_token(session_start)}, session_start,
		[&checked](auto ec)
		{
			REQUIRE_FALSE(ec);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	GenericToken sum_id;
	token_db.create_session(
		"SUMsum", session_start,
		[&checked, &sum_id](auto&& session, auto, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(session.valid());
			sum_id = session;
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	token_db.verify_session(
		sum_id, session_start + 500ms, [&checked, &sum_id](auto&& session, auto, auto ec) {
			REQUIRE_FALSE(ec);
			REQUIRE(session == sum_id);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Bad session key
	auto bad_key = std::to_array<std::uint8_t>({1, 2, 3});
	auto bad_session_key = GenericToken{sum_id}.set_entropy(ConstBuffer{bad_key});
	token_db.verify_session(
		bad_session_key, session_start + 500ms, [&checked](auto&& session, auto, auto ec) {
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Bad session index
	auto bad_session_index = GenericToken{sum_id}.set_sequence_number(sum_id.sequence_number() + 100);
	token_db.verify_session(
		bad_session_index, session_start + 500ms, [&checked](auto&& session, auto, auto ec) {
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Session expired after 25 hours (session length is 1 day)
	token_db.verify_session(
		sum_id, session_start + 25h, [&checked](auto&& session, auto, auto ec) {
			REQUIRE(ec == std::errc::permission_denied);
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Session in the past
/*	token_db.verify_session(
		sum_id, session_start - 1s, [&checked](auto&& session, auto ec) {
			REQUIRE(ec == std::errc::invalid_argument);
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
*/
	// Logout
	token_db.revoke_token(
		sum_id, [&checked](auto ec) {
			REQUIRE_FALSE(ec);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Session no longer valid
	token_db.verify_session(
		sum_id, session_start, [&checked](auto&& session, auto, auto ec)
		{
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Create another two more sessions 30 seconds "later"
	// Even with drifted TOTP token, login will be successful.
	auto later{session_start + 30s};
	GenericToken sess1, sess2;
	token_db.create_session(
		"SUMSUM", later, [&](auto&& session, auto, auto ec)
		{
			REQUIRE(session.owner() == "sumsum");
			REQUIRE_FALSE(ec);
			checked = true;
			sess1 = session;
		}
	);
	REQUIRE(RunFor(checked));
	token_db.create_session(
		"sumsum", later, [&](auto&& session, auto, auto ec)
		{
			REQUIRE(session.owner() == "sumsum");
			REQUIRE_FALSE(ec);
			checked = true;
			sess2 = session;
		}
	);
	REQUIRE(RunFor(checked));

	fmt::print("session start = {}\n", later);

	// Session about to expire, it should be renewed automatically. A different token will be returned.
	// It's a successor to the previous token (sess2).
	GenericToken successor;
	token_db.verify_session(
		sess2, later + refresh_after + 20s, [&](auto&& new_session, auto, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(new_session.owner() == "sumsum");
			REQUIRE(new_session.valid());
			REQUIRE(sess2 != new_session);
			REQUIRE(sess2.sequence_number()+1 == new_session.sequence_number());
//			REQUIRE(sess2.refresh() + 1 == new_session.refresh());
			checked = true;
			successor = new_session;
		}
	);
	REQUIRE(RunFor(checked));

	// Verify again with previous token (sess2), the same successor will be returned.
	// IOW, session will not re-renewed again.
	token_db.verify_session(
		sess2, later + refresh_after + 30s, [&](auto&& new_session, auto, auto ec) {
			REQUIRE_FALSE(ec);
			REQUIRE(new_session.owner() == "sumsum");
			REQUIRE(new_session.valid());
			REQUIRE(new_session == successor);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Revoke the successor token, and try to authenticate with the previous (sess2) token again.
	// Authentication will fail because both "successor" and "sess2" refers to the same session.
	token_db.revoke_token( successor, [&checked](auto ec)
	{
		REQUIRE_FALSE(ec);
		checked = true;
	});
	REQUIRE(RunFor(checked));
	token_db.verify_session(
		sess2, later + refresh_after + 35s, [&](auto&& new_session, auto, auto ec)
		{
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(new_session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
	token_db.verify_session(
		successor, later + refresh_after + 36s, [&](auto&& new_session, auto, auto ec)
	{
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(new_session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Destroy both sessions
	token_db.revoke_all_tokens(
		"sUMSUm", TokenType::session_id, [&checked](auto ec) {
			REQUIRE_FALSE(ec);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));

	// Both sessions no longer valid
	token_db.verify_session(
		sess1, later, [&checked](auto&& session, auto, auto ec) {
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
	token_db.verify_session(
		sess2, later, [&checked](auto&& session, auto, auto ec) {
			REQUIRE(ec == std::make_error_code(std::errc::permission_denied));
			REQUIRE_FALSE(session.valid());
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
}

TEST_CASE_METHOD(CommonFixture, "Token and parents", "[normal]" )
{
	auto redis = connect_redis();

	bool checked{};

	// Use a randomized database name to avoid timing issues from previous runs
	// It is possible to run into three-strike rejections if we run the UT too frequently
	auto db_name = std::string{"azuma"} + cppcodec::base64_rfc4648::encode(insecure_random(std::array<std::uint8_t, 5>{}));
	INFO("Set database name to " << db_name);

	std::chrono::seconds session_length{300};
	AuthenticationConfig session_config{.refresh_token_ttl=session_length, .access_token_ttl=10s};
	TokenDatabase token_db{redis, db_name, session_config};

	auto test_start     = std::chrono::system_clock::now();
	INFO("test start at " << fmt::format("{}", test_start));
	auto refresh_expiry = test_start + session_length;

	GenericToken refresh;
	token_db.create_refresh_token("yungyung", test_start, [=, &checked, &refresh](auto&& token, auto exp, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(token.valid());
		REQUIRE(token.type() == TokenType::refresh_token);
		REQUIRE(token.owner() == "yungyung");
		REQUIRE(exp == refresh_expiry);
		refresh = token;
		checked = true;
	});
	REQUIRE(RunFor(checked));

	GenericToken access1;
	token_db.refresh_token(
		refresh, test_start, GenericToken{refresh.owner(), TokenType::session_id},
		[=, &checked, &access1](auto&& session, auto exp, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(session.valid());
			REQUIRE(exp == test_start + session_config.access_token_ttl);

			access1 = session;
			checked = true;
		});
	REQUIRE(RunFor(checked));
	REQUIRE(access1.sequence_number() > refresh.sequence_number());

	token_db.get_children(refresh, 0, [=, &checked](auto&& children, auto ec)
	{
		REQUIRE(!ec);

		if (children.empty())
			checked = true;
		else
		{
			REQUIRE_FALSE(checked);
			REQUIRE(children.size() == 1);
			REQUIRE(children.front() == access1);
		}
	});
	REQUIRE(RunFor(checked));

	token_db.introspect(access1, test_start+9s, [=, &checked](auto exp, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(exp == test_start + session_config.access_token_ttl);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	token_db.introspect(access1, test_start+11s, [=, &checked](auto exp, auto ec)
	{
		REQUIRE(ec == std::errc::permission_denied);
		REQUIRE(exp == test_start + session_config.access_token_ttl);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	// Renew the session 5 seconds before session ends. The renewed session ID must expire in 5 seconds.
	GenericToken refreshed;
	token_db.renew_session(
		access1, test_start+295s, [=, &checked](auto&& token, auto exp, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(token.valid());
			REQUIRE(access1.owner() == token.owner());
			REQUIRE(access1.sequence_number() < token.sequence_number());
			REQUIRE(token.type() == TokenType::session_id);
			REQUIRE(exp == refresh_expiry);
			checked = true;
		}
	);
	REQUIRE(RunFor(checked));
}

TEST_CASE_METHOD(CommonFixture, "Housekeep tokens", "[normal]" )
{
	auto redis = connect_redis();

	bool checked{};

	// Use a randomized database name to avoid timing issues from previous runs
	// It is possible to run into three-strike rejections if we run the UT too frequently
	auto db_name = std::string{"azuma"} + cppcodec::base64_rfc4648::encode(insecure_random(std::array<std::uint8_t, 5>{}));
	INFO("Set database name to " << db_name);

	std::chrono::seconds session_length{300};
	AuthenticationConfig session_config{.refresh_token_ttl=session_length, .access_token_ttl=10s};
	TokenDatabase token_db{redis, db_name, session_config};

	auto test_start     = std::chrono::system_clock::now();
	INFO("test start at " << fmt::format("{}", test_start));
	auto refresh_expiry = test_start + session_length;

	GenericToken refresh;
	token_db.create_refresh_token("yungyung", test_start, [=, &checked, &refresh](auto&& token, auto, auto ec) mutable
	{
		REQUIRE_FALSE(ec);
		refresh = std::forward<decltype(token)>(token);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	std::vector<GenericToken> access;
	for (int i = 0; i<20; ++i)
	{
		token_db.refresh_token(
			refresh, test_start, GenericToken{refresh.owner(), TokenType::session_id},
			[=, &checked, &access](auto&& token, auto, auto ec) mutable
			{
				REQUIRE_FALSE(ec);
				access.push_back(std::forward<decltype(token)>(token));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}

	token_db.housekeep_tokens(refresh.owner(), TokenType::session_id, refresh_expiry, [&checked](auto ec)
	{
		REQUIRE_FALSE(ec);
		checked = true;
	});
	REQUIRE(RunFor(checked));
}
